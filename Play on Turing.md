# Play on Turing

To run Play on Turing, there's two small extra tasks we need to do:

1. Get you a unique port number, so your server doesn't clash with other students'
2. Tell the build tool how to deal with UNE's proxy.

Once those are done, Play is very nice and portable and works pretty smoothly.


## 1. Telling SBT how to deal with UNE's proxy

** *Only needed if you're working on turing* **

Under the hood, Play framework is written in Scala, but you can use it from Java or Scala. But that also means it uses `sbt` -- the Scala Build Tool -- to compile projects.

Like many modern build tools, Scala can fetch libraries from repositories on the web for us. In fact, it's so clever that actually the `sbt` script I've given you isn't even SBT itself. Instead, it's a little script that calls the SBT Launcher -- which goes and fetches the real SBT for you (so you always have the version you've configured your project to work with).

If you're on turing (our development server), SBT often gets blocked by UNE's proxy, and it doesn't pick up the `http_proxy` variables we set earlier. 

Instead, what we can do is tell SBT to look in a repository *inside* UNE's firewall for any dependencies it wants to get. So we have set up **Artifactory** on hopper. This is a library repository that can mirror the repositories out on the web, but that is accessible from inside UNE without going through the HTTP web proxy. But we need to tell SBT to use it.

In your home directory on turing, create a directory called `.sbt`. If it already exists, that's fine. We just need it to be there

```
mkdir -p ~/.sbt
```

Then:

```
cp ~cosc360/public_html/files/repositories ~/.sbt/
```

This will copy a file called `repositories` from the comp391 user into your `.sbt` directory.The `.sbt/repositories` file tells sbt which repositories to look in for libraries. The file I've given you tells sbt to look in Artifactory on hopper. You can open the file in a text editor if you'd like to see the contents -- there's not much there.

You might see some errors printed out at the console saying that your web proxy requires authentication -- that's SBT trying to go out to the Web before trying Artifactory on Hopper. So long as eventually the build is succcessful, don't worry about them. But if the build fails with an unresolved dependency, let me know.

    
## 2. Find out your port number

** *Only needed if you're working on turing* **

As there are lots of students on turing, we need to give each of you a unique port number

```
my_tomcat_ports
```

Note down the HTTP port you have been allocated. (We're not using Tomcat, but we'll use that port). 

Later in this tutorial, wherever you see **MYPORT**, replace it with your HTTP port number

    
## 3. Clone the base app

The developers of Play framework now provide a handy starter app, ideal for new developers. So let's clone those with git. 

First, let's go into our cosc360 directory. And make sure we've told *git* how to get through UNE's proxy

```
cd ~/cosc360
setproxy
```

Now, let's clone the repository

Java: 

```
git clone https://github.com/playframework/play-java-starter-example.git
```

or Scala:

```
git clone https://github.com/playframework/play-scala-starter-example.git
```

Next, cd into the directory that just got created, and let's disconnect your project from GitHub (because you might want to push it to your own repo in GitLab...)

```
cd play-java-starter-example
git remote rm origin
```



### Starting the server

let's have a look at what you've cloned

```
ls
```

You should see a relatively small number of files and directories.

* `project` contains some build files for sbt. Particularly, there are files setting the version of sbt we're using, and another setting the plugins we're using (the Play plugin)

* `build.sbt` contains the build definition for our project. There's not much in here yet

* `conf` contains configuration for our server. This includes the `routes` file that defines what valid paths on the server are, and what controller methods they call.

* `app` contains the code for our app. 

Let's start the server.

2. start sbt 

    ```
    sbt
    ```
    
    This will take a little while the first time, as it will go out to the internet to fetch parts of SBT itself, and then the Play framework that the project depends on. It will cache these in ~/.ivy and ~/.sbt, so next time it will be quicker
    
3. start the server

   ```
   run MYPORT
   ```
   
   remembering to replace MYPORT with your port number
   
   The server should appear to start very quickly -- it's actually waiting for an HTTP request to come in at which point it will compile itself. `run` is the development mode, where SBT constantly watches for code changes, and when a request comes in recompiles your code automatically if it needs recompiling.
    
    
### Curling our server


We're going to need to tell curl not to go through the web proxy for the requests we make to our own server.

Open a new shell to make these requests, but keep an eye on what's happening in the shell that is running Play

1. Call the server using curl

    ```
    curl -x "" -v localhost:MYPORT
    ```

    (remembering to replace $myport with your HTTP port number)
    
    When the request comes in, you should see messages about the code being compiled. 
    
    Even if you're using Java, you'll notice it says there is some Scala as well as some Java being compiled, even though there are no .scala files in the source code repository. Behind the scenes, Play generates some Scala code from the `routes` file, and it's this generated Scala code that it is referring to.

3. Open a web browser, and try visiting those URLs through the browser    

3. Have a look through all the application code. Does any of it look big or is it mostly small files with not much code in?
   
### Stop the server

1. Stop the server from running by `Ctrl-D`

2. Quit sbt by typing `Ctrl-D` again