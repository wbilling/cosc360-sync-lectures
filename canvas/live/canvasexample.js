"use strict";

var c = document.getElementById("thecanvas");
var ctx = c.getContext("2d");

ctx.translate(150, 0);
ctx.rotate(16 * Math.PI / 180);

ctx.beginPath();
ctx.strokeStyle = 'blue';
ctx.moveTo(20, 20);
ctx.lineTo(200, 20);
ctx.stroke();

ctx.beginPath();
ctx.arc(100, 100, 35, 0, 2 * Math.PI)
ctx.fillStyle = 'orange';
ctx.fill();

ctx.font = "48px monospace";
ctx.strokeText("Hello world", 0, 100);