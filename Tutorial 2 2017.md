# Tutorial 2 -- getting started with your favourite framework

First, choose your framework, and make sure you can get *their* sample app running. ie, follow the instructions from Tutorial 1b or 1c.

We'll use their code as a base, because occasionally I forget to upgrade the starter apps.

If you are working on Turing, don't forget you'll need to run 

```
setproxy
```

so that git knows how to get through the firewall.


## Start the server in developer mode

For **Sails.js** this is:

```
sails lift --dev
```

For **Play** this is:

```
sbt
run MYPORT
```

(`sbt` will bring SBT up to an SBT prompt. `run` then runs the server in dev mode. Where MYPORT is your unique number that you got in tutorial 1)

## Open the code in your favourite editor

For Play, I strongly recommend using IntelliJ. For Sails, any editor will probably do.

On turing, you can run IntelliJ IDEA using `idea.sh`. You might need to tell it a few things, such as which Java to use, and your proxy settings. Explore its menus to find these (you'll find yourself needing to navigate IntelliJ to make your coding experience smoother, so might as well start discovering it now!)

You should also install the Scala plugin if it is not already there, as this teaches IntelliJ IDEA how to understand SBT projects (and therefore Play projects).

If you are working on turing, I actually recommend opening projects by `cd`ing to the directory where your project is (go to the directory that has `build.sbt` in it). And then run `idea.sh .` (with the dot after it). This tells IntelliJ IDEA to open the current directory as a project. Turing has a lot of users, and if you go through IntelliJ's file browser to find your project (from the `Open...` menu), you could find it takes some time listing all the user directories first!


## Explore the code, and meet MVC

When the project is open, explore the code a little -- expand the `app` or `api` directory.

This weeks slides walk through the different parts of MVC, and in-class we'll talk through that in the tutorial itself. (There will be a video afterwards, but let's show you around in-class).

Once you've got a feel for where:

* The routes live
* The controller actions live
* The templates live

We're going to start building a sample app that does a Captcha -- a task people find easier to do than computers. This will probably take us two tutorials to write (you're exploring a new framework, so expect it to take you a little while to get comfortable knowing where to find things).


## The challenge

What we want to make the app do is:

* Keep a list of image URLs in memory, together with whether these pictures contain a beagle or not.
* We want a GET route "challange?pic=1234" that will go to a controller, that picks the pictures with those numbers, and passes them on to a form template
* The form template will show those pictures with HTML checkboxes. And a submit button, and the request to tick all the pictures that have beagles in them
* The form should submit to a POST route `challenge?pic=1234` that will compare what the user has selected with which pictures really did contain beagles. This should then return their score -- how many pictures did they correctly identify as containing a beagle or not.

