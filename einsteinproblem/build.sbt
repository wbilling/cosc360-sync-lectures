val versionStr = "0.1-SNAPSHOT"

val scalaVersionStr = "2.12.1"

lazy val veautiful = project.in(file("."))
  .enablePlugins(ScalaJSPlugin)
  .settings(
    name := "einsteinproblem",

    organization := "com.wbillingsley",

    version := versionStr,

    scalaVersion := scalaVersionStr,

    scalaJSUseMainModuleInitializer := true,

    testFrameworks += new TestFramework("utest.runner.Framework"),

    libraryDependencies ++= Seq(
        "org.scala-js" %%% "scalajs-dom" % "0.9.2",
        "com.lihaoyi" %%% "utest" % "0.4.5" % "test"
    )
)
