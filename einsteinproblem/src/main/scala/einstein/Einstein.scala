package einstein

import org.scalajs.dom

import scala.scalajs.js
import scala.scalajs.js.annotation.JSExportTopLevel

import org.scalajs.dom.webworkers.DedicatedWorkerGlobalScope.self

/** This is our web worker app, that is going to solve the problem for us */
object Einstein extends js.JSApp {

  /** We keep a log of all the HTML we've printed. This is "posted" to the page, which displays it. */
  val log = new StringBuilder()

  /**
    * When we want to write some HTML into the page, we append it to the log and post a message.
    * The page handles inserting this HTML into our div.
    */
  def writeHtml(s:String) = {
    log.append(s)
    self.postMessage(log.mkString, js.undefined)
  }

  /*
   * Now let's set up the values that are going to be used in the riddle.
   * We'll just define them as Symbols -- we're not going to distinguish the type of each. That
   * way we can just ask for rowOf(symbol) and not have to worry about whether it's a nationality or a drink.
   */
  val nationalities = Seq('Englishman, 'Swede, 'Dane, 'German, 'Norwegian)
  val drinks = Seq('tea, 'water, 'coffee, 'milk, 'beer)
  val colours = Seq('red, 'blue, 'white, 'green, 'yellow)
  val smokes = Seq('Dunhill, 'PallMall, 'Blend, 'BlueMasters, 'Prince)
  val pets = Seq('birds, 'cats, 'dogs, 'horses, 'fish)

  /** A sequence of all the symbols -- we use this in the constraint that each symbol must appear once. */
  val allSymbols = nationalities ++ drinks ++ colours ++ smokes ++ pets

  /** In our grid, Cells are a list of the symbols they could still contain. */
  type Cell = Seq[Symbol]

  /** A row has a sequence of cells, but we also remember the row number so we can do "leftOf" easily */
  case class Row(number: Int, cells:Seq[Cell]) {

    /** The row is solved if all its cells only have one possible value remaining */
    def solved = cells.forall(_.size == 1)

    /** How many cells in this row could yet contain this symbol? */
    def countPossibilities(s:Symbol):Int = {
      cells.count(_.contains(s))
    }

    /** Does this row still contain this symbol as one of its possibilities? */
    def contains(s:Symbol):Boolean = {
      cells.exists(_.contains(s))
    }

    /** Is the symbol "set" in this row -- that is, is there a cell where this is the only possible value? */
    def whereSet(s:Symbol):Boolean = {
      cells.exists((c) => c.contains(s) && c.size == 1)
    }

    /** Copy this row, but set a column to contain only the specified symbol */
    def updated(index:Int, value:Symbol):Row = {
      copy(cells = cells.updated(index, Seq(value)))
    }

    /** Copy this row, but eliminate the specified symbol from a column */
    def remove(index:Int, value:Symbol):Row = {
      copy(cells = cells.updated(index, cells(index).filterNot(_ == value)))
    }

    /** Pretty print the row for text printing */
    def pretty:String = {
      cells.mkString(",\t")
    }

    /** Pretty print a cell for HTML */
    def prettyHtmlCell(cell:Seq[Symbol]):String = {
      cell.mkString("<br/>")
    }

    /** Pretty print the row for HTML */
    def prettyHtml:String = {
      s"<td>${number}</td><td>" + cells.map(prettyHtmlCell).mkString("</td><td>") + "</td>"
    }

  }

  /**
    * A grid is a sequence of (five) rows
    */
  case class Grid (rows:Seq[Row]) {

    /** The grid is solved if every row is solved */
    def solved = rows.forall(_.solved)

    /** Find a row where s has been "set" (is the only possible value remaining for a cell) if there is one */
    def rowOf(s:Symbol):Option[Row] = {
      rows.find(_.whereSet(s))
    }

    /** Get the row with n as an index, but return None if n is out of range, rather than throwing an Exception */
    def getRow(n:Int):Option[Row] = {
      if (rows.indices.contains(n)) Some(rows(n)) else None
    }

    /** Finds the first failing proposition (or None) for this grid */
    def violation(p:Seq[Prop]):Option[Prop] = {
      p.find(!_._2.apply(this))
    }

    /** How many possible rows could this symbol still be in? */
    def countPossibilities(s:Symbol):Int = {
      rows.map(_.countPossibilities(s)).sum
    }

    /** Checks that if there is a row where a is set (single value), then one of its neighbours could still contain b */
    def leftCanNeighbourRight(a:Symbol, b:Symbol):Boolean = {
      rowOf(a).forall({
        r => getRow(r.number + 1).exists(_.contains(b)) || getRow(r.number - 1).exists(_.contains(b))
      })
    }

    /** Checks that if a has been "set" in a row, the row to the right of it could contain b */
    def _leftOf(a:Symbol, b:Symbol):Boolean = {
      rowOf(a).forall({
        r => getRow(r.number + 1).exists(_.contains(b))
      })
    }

    /** Checks that if a has been "set" in a row, the row to the left of it could contain b */
    def _rightOf(a:Symbol, b:Symbol):Boolean = {
      rowOf(a).forall({
        r => getRow(r.number - 1).exists(_.contains(b))
      })
    }

    /** Reflexive. Checks that a can be leftOf b and b can be right of a */
    def leftOf(a:Symbol, b:Symbol):Boolean = _leftOf(a, b) && _rightOf(b, a)

    /** Reflexive, checks whether a could neighbour b, and b could neighbour a */
    def neighbour(a:Symbol, b:Symbol):Boolean = {
      leftCanNeighbourRight(a, b) && leftCanNeighbourRight(b, a)
    }

    /** Checks that if there is a row where a is set (single value), it could also contain b */
    def alsoCanHave(a:Symbol, b:Symbol):Boolean = {
      rowOf(a).forall(_.contains(b))
    }

    /** Reflexive. If either a or b is set in a row, the other must be possible */
    def sameRow(a:Symbol, b:Symbol):Boolean = {
      alsoCanHave(a, b) && alsoCanHave(b, a)
    }

    /** Copy this grid, but set a particular row to a new value */
    def updated(row:Row):Grid = {
      Grid(rows = rows.updated(row.number, row))
    }

    /** Copy this grid, but set a particular cell to have only one possible value */
    def withValue(row:Int, col:Int, value:Symbol):Grid = {
      updated(rows(row).updated(col, value))
    }

    /** Copy this grid, but eliminate a value from a cell. */
    def eliminating(row:Int, col:Int, value:Symbol):Grid = {
      updated(rows(row).remove(col, value))
    }

    /**
      * An iterator that cycles through each grid cell setting each value.
      * By using iterators, we can create nested loops, but "find" will exit efficiently
      * when it finds the first one.
      */
    def assignmentIterator:Iterator[AssignedGrid] = {
      for {
        r <- rows.iterator
        c <- r.cells.indices.iterator
        v <- r.cells(c).iterator
      } yield {
        AssignedGrid(r.number, c, v, withValue(r.number, c, v))
      }
    }

    /**
      * Eliminate any values in the grid that are currently violating a constraint.
      */
    def eliminateImpossibilities(props:Seq[Prop]):Grid = {
      writeHtml(
        """
          | <h4>Eliminate values that would immediately violate a constraint</h4>
        """.stripMargin)

      /*
       * Although we will just check the constraints on this grid, we keep a "cursor" for when we eliminate the values
       * one-by-one.
       */
      var cursor = this

      /*
       * Iterate across all possible value-assignments in this grid, picking only those where violations resolves to be
       * non-empty. We'll eliminate these values from the grid we return.
       */
      for {
        assigned <- assignmentIterator
        (message, _) <- assigned.grid.violation(props)
      } {
        println(s"$message ==> ${assigned.row} is not ${assigned.value}")
        writeHtml(
          s"""
             | <p>$message ==> ${assigned.row} is not ${assigned.value}</p>
           """.stripMargin)

        // Eliminate this value from the grid -- it's violated a constraint
        cursor = cursor.eliminating(assigned.row, assigned.col, assigned.value)
      }

      writeHtml(
        s"""
           | <p>At the end of this iteration through the grid, we have:</p>
           | ${cursor.prettyHtml}
         """.stripMargin)

      cursor
    }

    /** Finds the first legal move for a grid */
    def findViableMove(props:Seq[Prop]):Option[AssignedGrid] = {
      assignmentIterator.find(_.grid.violation(props).isEmpty)
    }

    /** Looks ahead 1 move for dead-ends */
    def deadEnd1(props:Seq[Prop]):Option[AssignedGrid] = {
      // From the assignment-iterator, find the first value-setting which would then have no legal moves
      assignmentIterator.find(_.grid.findViableMove(props).isEmpty)
    }

    /** Looks ahead 2 moves for dead-ends */
    def deadEnd3(props:Seq[Prop]):Option[AssignedGrid] = {
      assignmentIterator.find((a) => {
        a.grid.assignmentIterator.forall((b) => {
          b.grid.assignmentIterator.forall((c) => {
            c.grid.findViableMove(props).isEmpty
          })
        })
      })
    }

    def lookAhead1(props:Seq[Prop]):Grid = {
      var cursor = this
      for {
        assigned1 <- deadEnd1(props)
      } {
        writeHtml(
          s"""
             | <p>Setting ${assigned1.row} to ${assigned1.value} is a dead end (all next steps violate)</p>
           """.stripMargin)
        println(s"Setting ${assigned1.row} to ${assigned1.value} is a dead end (all next steps violate)")
        cursor = cursor.eliminating(assigned1.row, assigned1.col, assigned1.value)
      }
      cursor
    }

    def runEliminations(props:Seq[Prop]):Grid = {
      var cursor = this
      var before = this

      do {
        before = cursor
        cursor = cursor.eliminateImpossibilities(props)
      } while (cursor != before)

      cursor
    }

    def pretty:String = {
      val sb = new StringBuilder
      for { row <- rows } sb.append(row.pretty + "\n")
      sb.toString
    }

    def prettyHtml:String = {
      val sb = new StringBuilder
      sb.append(
        """
          | <table class="table table-striped table-bordered table-sm">
          | <thead>
          |   <tr>
          |     <th>#</th>
          |     <th>Nationality</th>
          |     <th>Drink</th>
          |     <th>Colour</th>
          |     <th>Smokes</th>
          |     <th>Pet</th>
          |   </tr>
          | </thead>
          | <tbody>
        """.stripMargin)
      for { row <- rows } {
        sb.append("<tr>")
        sb.append(row.prettyHtml + "\n")
        sb.append("</tr>")
      }
      sb.append(
        """
          | </tbody>
          | </table>
        """.stripMargin)
      sb.toString
    }

  }

  /** Holds a tentative grid with a value set */
  case class AssignedGrid(row:Int, col:Int, value:Symbol, grid:Grid)



  /**
    * Our starting grid, where each cell contains all the possibilities
    */
  val startingGrid:Grid = Grid(for { i <- 0 until 5 } yield Row(i, Seq(nationalities, drinks, colours, smokes, pets)))

  /** A proposition is a tuple containing a name and test to run on the Grid */
  type Prop = (String, Grid => Boolean)

  /** All 17 propositions in the puzzle */
  val propositions:Seq[Prop] = Seq(
    "No value appears more than once" -> { (g) =>
      // Get the cells that have been "set" (have only one value remaining)
      val setCells:Seq[Symbol] = g.rows.flatMap(_.cells).filter(_.size == 1).map(_.head)

      // For all the set cells, the value should appear exactly once in the set cells (ie, not "set" twice)
      setCells.forall(s => setCells.count(_ == s) == 1)
    },

    "All values must appear once" -> { (g) =>
      allSymbols.forall((s) => g.countPossibilities(s) >= 1)
    },

    "Englishman lives in the Red house" -> { _.sameRow('Englishman, 'red) },

    "Swede keeps dogs" -> { _.sameRow('Swede, 'dogs) },

    "Dane drinks tea" -> { _.sameRow('Dane, 'tea) },

    "Green house to left of white house" -> { _.leftOf('green, 'white) },

    "Owner of green house drinks coffee" -> { _.sameRow('green, 'coffee) },

    "Pall Mall smoker keeps birds" -> { _.sameRow('PallMall, 'birds) },

    "Owner of yellow house smoke Dunhill" -> { _.sameRow('yellow, 'Dunhill) },

    "Man in centre house drinks milk" -> { (g) => g.getRow(2).forall(_.contains('milk)) },

    "Norwegian lives in the first house" -> { (g) => g.getRow(0).forall(_.contains('Norwegian)) },

    "Blend smoker has a neighbour who keeps cats" -> { _.neighbour('Blend, 'cats) },

    "Man who smokes BlueMasters drinks beer" -> { _.sameRow('BlueMasters, 'beer) },

    "Man who keeps horses lives next to the Dunhill smoker" -> { _.neighbour('horses, 'Dunhill) },

    "German smokes Prince" -> { _.sameRow('German, 'Prince) },

    "Norwegian lives next to blue house" -> { _.neighbour('Norwegian, 'blue) },

    "Blend smoker has a neighbour who drinks water" -> { _.neighbour('Blend, 'water) }

  )


  /** Main function of the webworker. */
  def main():Unit = {
    // Post a loaded message to the page
    self.postMessage("<p>Script loaded...</p>", js.undefined)

    // run the solver immediately
    solveIt()
  }

  /**
    * Our fabulous solver...
    */
  def solveIt(): Unit = {
    writeHtml(
      s"""
         | <p>Here's what our grid looks like to begin with:</p>
         | ${startingGrid.prettyHtml}
       """.stripMargin)

    // Start the clock
    val time = System.currentTimeMillis()

    writeHtml(
      s"""
         | <h3>Let's solve the riddle... Ready, set, start the clock!</h3>
       """.stripMargin)

    /*
     * Time to solve the riddle. We alternate between looking for entries we can eliminate
     * immediately, and searching for dead-ends to eliminate (with the hand-picked lookahead
     * value of 2)
     */
    var cursor = startingGrid
    var before = cursor
    do {
      before = cursor
      cursor = cursor.runEliminations(propositions)
      println(cursor.pretty)

      if (!cursor.solved) {
        println("Not solved yet. Looking for moves that have no viable two-move follow-ups")
        writeHtml(
          """
            | <h4>Not solved yet. Look for dead-ends to eliminate, with a look-ahead of two.</h4>
          """.stripMargin)
        cursor = cursor.lookAhead1(propositions)
        println(cursor.pretty)
        writeHtml(cursor.prettyHtml)
      }

    } while (!cursor.solved && before != cursor)

    if (cursor.solved) {
      for { row <- cursor.rowOf('fish) } {
        println(s"The ${row.cells(0)(0)} owns the fish")
        writeHtml(
          s"""
             | <p class="lead">The ${row.cells(0)(0)} owns the fish</p>
             | <p>Solved in ${(System.currentTimeMillis() - time)} milliseconds
           """.stripMargin)
      }
    } else {
      writeHtml(
        s"""
           | <p class"lead">Oh no, we didn't solve it!</p>
         """.stripMargin)
    }

  }


}
