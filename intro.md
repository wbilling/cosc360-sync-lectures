class: center, middle

## Advanced Web Programming
(COSC360/560)

---

### What's covered

* At the end of this unit, you should be able to build (small) modern web systems that do something useful. Think web start-ups…

--

* DNS, HTTP, HTTPS, Server Sent Events, WebSockets, OAuth

--

* ReST, server-side MVC, writing JSON APIs

--

* CoffeeScript, TypeScript

--

* “Single Page Apps”, React.js, Angular.js, Vue.js, Scala.js

---

### This is a studio-style unit

* Each student will be working on a project *of their own*. 

* You'll be asked to reflect on *your own* work *and on your classmates' work*.


---

### Assessment

* CAUC is law. If there's a grade percentage on CAUC, it is right.

  [COSC360](https://my.une.edu.au/courses/2016/units/COSC360) and [COSC560](https://my.une.edu.au/courses/2016/units/COSC560)
  
--

* The project runs all term. **It has checkpoints**

--

* Project-based social learning experience. You are building something individually, but you are not alone.

--

* You will have to produce **videos** at various stages. And **critique videos**. 

--

* You will also have to produce a video demo of your project

---

### COSC360 vs COSC560



---

## Lectures

* Mostly via video. Many of them will be live demos.

--

* Notes will usually follow the video. Feel free to screenshot videos and post your own though.

--

* The whole course will also go up on GitHub Pages. URL to be posted later.



---

## Tutorials

* You'll be sharing the same room with COSC220 Software Engineering Studio. You'll also be sharing some starter code...

--

* To start with, the tutorials will focus on showing you how to do something

--

* Later, I expect you'll mostly want to apply what I'm showing you in your projects. (I'll skew the lecture videos to demonstrations, and use the tutorial time to help you with problems)


---

class: center, middle

# Network effects


---

## Network Effects

The first phonebook?

--

```
1. Bell, Alexander Graham
2. Watson, Thomas A.
```

--

![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/10/Alexander_Graham_Bell.jpg/220px-Alexander_Graham_Bell.jpg) 
![](https://upload.wikimedia.org/wikipedia/en/thumb/f/f8/Thomas_watson.jpg/200px-Thomas_watson.jpg)

---

### Metcalfe's Law

> The value of a network is proportional to the square of the number of users

Applies to telephones, internet, Web, social networks...

--

### A consequence

A high value network is *very hard to displace*

--

How hard would it be to displace Facebook?

---

![](cosc360/1/fb gplus.png)


---

![](cosc360/1/instagram headline.png)

* In 2012, Facebook bought Instagram (photo-sharing app) for $1bn  

--

* Before 2012, Facebook was mostly used on desktop computers.
  
--

* Instagram added 130,000 new users per week on mobile (Mar 2011).

--

* Fear that move to mobile would displace their market?

---

![](cosc360/1/mobile.png)


---

### Network effects and the Web

--

* HTTP is an old technology. 1.1 released in 1999. *Only just replaced!*

  Designed for [*hypertext*](https://en.wikipedia.org/wiki/Hypertext)

--

* JavaScript is an *old* technology, designed for *little animations*

--

* But everyone's browser talks HTTP, and everyone's browser has JavaScript

--

* So everyone's server talks HTTP, and everyone's server serves up JavaScript

--

* ***Two sided network effect***

---

### TLDR;

* We're trying to build modern, interactive, real-time applications...

* ...on a platform that was fundamentally designed for 1990-era static hypertext

--

> **expect a certain amount of awkwardness!**

---

### The good news

* It is probably the most inspectable programming environment you will encounter. Everything is text, and you already have some debugging tools...

--

* *Abstractions are your friend*. Small things working together.


