class: center, middle

# Cookies and Sessions

---

### State 

* Many (non-web) programs operate like state machines

  ![state machine](https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/CPT-FSM-abcd.svg/326px-CPT-FSM-abcd.svg.png)

---

### HTTP is stateless

* Each request contains information needed to process it
* Does not depend on transient state on the server (or the sequence of previous requests)
* eg, in tutorial 3:
    * Server sent a (random) set of five pictures to the browser. User is asked to select *all the pictures containing beagles*.
    * But when the client submits the form, the server *doesn't remember* which pictures it chose to send

---

### The problem 

The client says to the server

> Pictures 1 and 3 contained beagles.

Is that all of them? The server doesn't remember which photos it chose to send to know if it had included any more beagle pictures.

---

### State in a stateless protocol

We can add functionality to remember the pictures

* in the form
* in the URL
* otherwise at the client, or
* otherwise at the server

---

### Tutorial 3 naive solution

In tutorial 3, I suggested putting it into a set of hidden fields on the form. Then the HTTP form response is effectively 

> You gave me these five pictures, and these three contained beagles

* Good HTTP practice -- the request contains everything the server needs to know to process it.

---

### The problem with the naive approach

* Bad design for a Captcha -- the client can just fake the response. 

    * **Server**: *Here are pictures 1, 2, and 3. Which ones contain beagles?*  
        **Client**: *You sent me pictures 1, 2, and 3. 2 contains a beagle.*  
        **Server**: *Correct*.

    * **Server**: *Here are pictures 4, 5, and 6. Which ones contain beagles?*  
        **Client**: *You sent me pictures 1, 2, and 3. 2 contains a beagle.*  
        **Server**: *Correct*.

---

### Naive solution at the server

* We could create a stateless solution on the server.

* eg, each Captcha gets a URL based on which photos are included

      `http://localhost:9000/captcha/1,2,3,4,5` contains photos 1, 2, 3, 4, and 5
      
* But then the client can just choose a Captcha it already knows the answer for 

---

### So what do we want

* The server issues the challenge

* The client must post its answer to the challenge **that the server chose**

* We're designing a stateful app using a stateless protocol

* There are several ways of doing this

---

### The server shouldn't change state, but the resources can

1. When the client makes a `POST` to `/challenge`, the server creates a new challenge containing five pictures. 

    It gives this challenge a unique, long, unguessable number (a UUID)

2. The form `action` includes the `UUID`. eg:  

    ```html
    <form action="/challenge/c7ef89afbcd154923e" method="post" >
    ```
    
3. When the challenge is attempted, the server processes the response and deletes the challenge.

---

### Changing resource solution...

* The requests contained everything needed to process it

* Only resources changed state (and they have to be able to -- eg "author's preferred version of a paper")

**But...**

* We have to remember that challenge resource ID. Ok this time, but imagine if our resource was a shopping cart not a one-time-challenge.
 
---

### Shopping carts, if we kept it stateless

* Dozens of links on the page

* Every link is a stateless request. Views on products look like they should be `GET` requests.

* If we don't include the cart's resource ID in a single URL, then following that URL forgets the cart.

Wouldn't it be easier just to remember the cart's ID separately from the URL, so the URLs could be the same for every customer?

---

### Cookies. Not so stateless.

* A Cookie is a variable name and value that the client can remember between requests

* If an HTTP response contains the header `Set-Cookie` it will set the value:
 
    ```http
    Set-Cookie: challengeId=c7ef89afbcd154923e; Path=/challenge; Secure
    ```
   
    sets the variable `ChallengeId` to the value `c7ef89afbcd154923e`

* The Cookie will be sent by the client in the `Cookie` request header in future requests to the same origin (server, port, and protocol)

    ```http
    Cookie: challengeId=c7ef89afbcd154923e; otherCookie=otherValue
    ```

* Neither a cookie's name nor value should contain whitespace or any of the following characters: `[]()=,"/?@:;`

    (Use a character escaping/encoding scheme if you need to)
    
---

### Cookie attributes

* **Domain**: Use this to set whether a cookie should be sent for sub-domains. eg, whether a cookie on `example.com` should be sent to `www.example.com`
* **Path**: Which paths (and their subpaths) to send the cookie to. Can be `/`
* **Expires**: A date after which the browser should delete the cookie
* **Max-Age**: A duration after which the browser should delete the cookie
* **Secure**: Only send the cookie over secure connections, eg HTTPS
* **HttpOnly**: Only expose the cookie over HTTP and HTTPS, not other protocols, and not Javascript (`document.cookie`)

Cookies with no `Expires` or `Max-Age` are *session cookies*. To the browser this means, they disappear when the user closes the browser.

---

### Shopping carts and cookies

* We can now treat the shopping cart as a changing resource, but put its ID in a cookie instead of in the request path.


---

### Roy Fielding's objection to Cookies

* Cookies mean that a sequence of GET requests might not be safe or idempotent

* Suppose a browser makes these GET requests:

   1. `GET /printCookies` (and the server shows what cookies were sent)
   2. `GET /foo` (and the response sets a cookie)
  
    and then the user presses "Back" in their browser.
    
    Will the response for `GET /printCookies` after the request to `GET /foo` be the same?

---

### Limitations with cookies

* Per-browser, whereas the user might have multiple browsers, on multiple machines

* Sent by the client. eg, you could make up a cookie value in a request using curl

* Forgettable, eg user can clear cookies

* Just a header -- security depends on security of transport (see later)

---

### Typical use

* We generally want to keep most important state on the server
* When user visits a site, we give them a session id in a session cookie. 

    eg: session=e873f012abc9fa
    
* We store data on the server, and can use the session key to look it up.

* **This includes the user id. Don't push the user id into a cookie.**

---

### Why not put userId in a cookie?

* Suppose we 

    ```http
    Set-Cookie: userId=algernonMoncrieff; Secure; HttpOnly
    Set-Cookie: verifier=134ef91abc; Secure; HttpOnly
    ```
    
    and the user was using a public machine at the airport and forgot to log-out before they boarded a plane. How could they log out the session?
    
* Instead, give them a disposable session key (in a session cookie), and associate the session key with the user on the server (or in the db)

* We can then revoke the session key just by altering the data on the server

* We can also hold useful information about the session. eg, IP address, last connection time, etc.

---

### Session cookies in Play

* Play automatically creates a session cookie
* It signs it with a verifier to be sure it hasn't been tampered with at the client
* This "session scope" is handled in the ThreadLocal context, and simple methods are available on controllers to put variables and values into Play's session cookie.

```java
session("myVar", "value");
```

```java
String myVar = session("connected");
```

---

### Firesheep

* Beware of sending cookies (including session cookies) over insecure HTTP (rather than secured HTTPS)

* Firesheep

    * Listened for HTTP requests over public WiFi
    * Stole session cookies for Facebook, Google, etc
    * Gave an easy interface to set that cookie in your browser (become logged in as them on Facebook!)
    * Demonstrator to stir Facebook, Google, etc into going HTTPS-only.
    * [Eric Butler's slides](http://codebutler.github.io/firesheep/tc12/#40)

* But sorry, you can't use HTTPS on turing (we haven't set up SSL certificates for you).

