# Futures and Promises tutorial

Clone the tutorial repository from GitHub, and open it in IntelliJ

```
setproxy
git clone https://github.com/UNEcosc250/futuresAndPromisesTutorial.git
idea.sh 
```


### Libraries you've been given this time

I've given you three a few extra libraries this time:

```scala
libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.4.0",
  "com.typesafe.play" %% "play-ws" % "2.5.0",
  "com.typesafe.play" %% "play-json" % "2.5.0",
  "org.scalactic" %% "scalactic" % "2.2.6",
  "org.scalatest" %% "scalatest" % "2.2.6" % "test"
)
```

*Akka* is an Actor framework. We'll find out about that next week. Just know that it's lurking behind the scenes even though we're not interacting with it directly.

*play-ws* is the simple asynchronous HTTP client we used in class. Behind the scenes, it uses Akka's HTTP client (hence AhcClient in the code), but with a much simpler interface

*play-json* is a parser for JSON data. JSON data looks like this:

```json
{
  "myField": "myValue",
  "myArray": [ { "myField": "myValue" }, { "myField": "another" } ]
}
```

### What you'll be doing

As usual, there is a code file and a test file, and the aim is to make the tests pass.

And as usual, it's been written in a bit of a hurry so if there's an update to the project in the tutorial you might find this sequence useful:

```
git stash
git pull
git stash pop
```

This stashes your changes, so you can then do a pull (to fetch any updates to the base project I've published), and then re-applies your changes.


### Asynchronous tests

Because we're dealing with futures, our testing framework has to deal with a problem.

If your function returns *a computation that will complete in the future*, how does it check its result? (It won't have finished when we reach the testing line)

So, we have to tell ScalaTest to wait for the result to be produced and then check it. This is done with a call to `futureValue` -- a blocking call that will wait for the result, that comes from `org.scalatest.concurrent.ScalaFutures`


```scala
class Courses2016Spec extends FlatSpec with Matchers with ScalaFutures {

//...
        myResult.futureValue should be ("success")
        
//...

}        
```