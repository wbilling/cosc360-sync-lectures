# Tutorial 2 -- getting started with Play

In this tutorial, we're going to get started with Play framework, working with Routes, responses, JSON, and 

## Get the code


1. Connect to turing.une.edu.au, either via nx (NXClient, x2go, etc) or via ssh, and open a terminal

2. Set your proxy settings for unix utilities

    ```
     setproxy 
    ```

3. Clone the starting project for this tutorial.

    ```bash
    git clone https://github.com/UNEadvancedweb/tutorial_routes_controllers
    ```

4. cd into the directory that cloning the repository has created, and start sbt

   ```bash
   ./sbt
   ```
   
5. Run the server

   ```
   run $myport 
   ```

6. Open the browser, open its developer tools and connect to your server

   ```
   turing.une.edu.au:$myport
   ```

   And have a little look at the request in the developer tools

## Open IntelliJ

On turing, you can run IntelliJ IDEA using `idea.sh`. You might need to tell it a few things, such as which Java to use, and your proxy settings. Explore its menus to find these (you'll find yourself needing to navigate IntelliJ to make your coding experience smoother, so might as well start discovering it now!)

You should also install the Scala plugin if it is not already there, as this teaches IntelliJ IDEA how to understand SBT projects (and therefore Play projects).

If you are working on turing, I actually recommend opening projects by `cd`ing to the directory where your project is (go to the directory that has `build.sbt` in it). And then run `idea.sh .` (with the dot after it). This tells IntelliJ IDEA to open the current directory as a project. Turing has a lot of users, and if you go through IntelliJ's file browser to find your project (from the `Open...` menu), you could find it takes some time listing all the user directories first!


## Explore the code, and meet MVC

When the project is open, explore the code a little -- expand the `app` directory.

You should see two packages -- `model` and `controllers`. This is part of the Model View Controller pattern that is common to a lot of web frameworks. Roughly, the idea is that when you are writing your core code remember that you are just programming. How would you write it if it wasn't an HTTP client but some other kind of client that was calling your API? That's your model. The controllers package then holds all of the "controllers" that respond to HTTP requests and produce HTTP responses. And they call your model API.

We'll meet views later...



1. The code for showing a set of pictures is already there ... but there's no route pointing to it.

   Create a GET route to `controllers.CaptchaController.showPictures`. You'll need to edit `conf/routes`. Use the examples already in the file to guide you.
   
   Now visit your route in the browser. At the sbt command line, you should see Play recompiling your code for the new route.
   
   When it's loaded, right-click and inspect the images. You should notice that some of the URLs are `http:` and some are `data:`. What do you think the difference is?


2. Let's add a form around it. If you need to read up on how HTML Forms work, the [w3schools.com tutorial](http://www.w3schools.com/html/) isn't bad. Or there's an example of a form in the code too.

   Wrap the HTML produced in `<form>` tags, and put a checkbox input under each picture, for the user to check if it's a beagle.  An example checkbox might look like
   
   ```html
   <input type="checkbox" name="beagle" value="1" /> <label>It's a beagle!</label>
   ```
   
   And add a submit button to your form
   
   ```html
   <button type="submit">Submit</input>
   ```

3. Refresh the page (correcting any errors). Tick a few beagles, and inspect the request in the Network tab of the browser's developer tools.

   What kind of request was it?
   How was it encoded?
   What were the request parameters?
   What was the response?
   
4. Right now, your page is probably submitting a GET request to the same URL, so you're just getting your form back again. Change the form's method to `POST` and try again

   ```html
   <form method="POST">
   ```

   Refresh, click submit, and inspect the request.
   
   How was the data encoded?
   Why did you get a 404 Not Found response?

5. Our POST request needs a new route. I've already written a stub function that will get the contents of the post request. Wire it up, and try again.

   Now the next problem is you can tell the beagles you got right, but anything you didn't tick wasn't sent back at all.
   
   Add a hidden input under each beagle so you know what was sent. For example,
   
   ```html
   <input type="hidden" name="sent" value="1" />
   ```

6. There's also a stub of a JSON controller, that would return all of the beagles and non-beagles in a JSON format. Complete the function, wire it up to a route, and give it a go.

   Inspect the request. How did we set the MIME type (Content-Type) header in the response?

7. Let's try a different kind of response. Play's router also generates reverse routes -- a way your code can find out the URL that a controller is wired up to. Have a read on this here: 

    [https://www.playframework.com/documentation/2.5.x/JavaRouting](https://www.playframework.com/documentation/2.5.x/JavaRouting)

   and then wire up a route called "/redirect" so it will produce a redirect response to your beagle page. 
   
   **NB:** The reverse routes classes are generated from your `conf/routes` file when the code is recompiled. And the code is recompiled when a request comes in after you have edited a file. This means after you edit the routes file, you might need to hit refresh in your browser to regenerate the routes before you can refer to those reverse routes in your controller. 

Ok, if you've got this far, you should have played with some of the basic machinery that Play offers -- wiring up routes to controllers, and making it simple to set headers and return particular kinds of responses.

And if you haven't got this far, I will post a video working through this, and a solution branch on GitHub.

In future tutorials, we'll start to make this a bit more useful.

   
### Stop the server

1. Stop the server from running by `Ctrl-D`

2. Quit sbt by typing `Ctrl-D` again