class: center, middle

# WebGL

---

## WebGL and Canvas

* So far, we've seen Canvas used to draw 2d graphics

* With WebGL we can draw 3d graphics, *hardware accelerated* -- ie, you're writing code to run on your GPU as well as your CPU

* Renders into a `<canvas>` element

---

## This video

* We're going to talk through this example:  
  http://www.sw-engineering-candies.com/snippets/webgl/hello-world

* 500 lines of code (WebGL is a bit verbose...)

* Remember the principals, not the code

---

## Canvas has a context

Just as our asteroids Canvas code fetched a "2d" context, webgl also has a context

```js
canvas = document.getElementById("glcanvas");
```

and inside `initWebGL(canvas)`:

```js
gl = canvas.getContext("experimental-webgl");
```

---

## Setting GL context properties

```js
gl.clearColor(0.0, 0.0, 0.0, 1.0);  // Clear color is opaque black  
gl.clearDepth(1.0);                 // Clear everything
```
   
```js              
gl.enable(gl.DEPTH_TEST);           // Enable depth testing
gl.depthFunc(gl.LEQUAL);            // Near things obscure far things
```


---

## In general

* Graphics cards know how to render "triangles in space"

* **Vertices** are the corners of the triangles

* **Vertex buffers**: You'll want to upload all the points in your geometry. For example, in a cube, the faces have corners in common.

* **Vertex index buffers**: The triangles are then formed by selecting three of the vertices by their indexes.

---

## Creating buffers

```js
  cubeVerticesBuffer = gl.createBuffer();
  
  // Select the cubeVerticesBuffer as the one to apply vertex
  // operations to from here out.
  gl.bindBuffer(gl.ARRAY_BUFFER, cubeVerticesBuffer);
  
  // Now create an array of vertices for the cube.
  var vertices = [
    // Front face
    -1.0, -1.0,  1.0,
     1.0, -1.0,  1.0,
```
```     
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
```

---

## The buffers in the example

* `vertices` -- the vertices of the cube
* `vertexNormals` -- which way each vertex points, used for calculating lighting
* `textureCoordinates` -- maps the texture onto the vertices
* `cubeVertexIndices` -- each face is made up of two triangles. These are indexes into the `vertices` array to get the points of each triangle. 


---

## Shaders

"Shaders" are a bit of a misnomer. They are routines that you upload into the graphics card to help render the scene. They are not just about adding shade.

* **Vertex shaders** are routines that *can adjust the vertices*. This could include moving them in space, or setting attributes on them (often a "lighting" value).

* **Fragment shaders** are routines that work out what colour to paint a pixel on a triangle. A fragment shader might look up a point in a **texture** to choose the colour for a pixel. (Or it could do something else)

---

## Shaders aren't JS

* Your shaders *run in the GPU*. It doesn't know how to execute JavaScript. 

* Shaders are written in a shader language (ES) and have to be compiled and uploaded to the graphics card.

* Your JavaScript compiles and uploads the shaders!

---

## Shaders are in GLSL

```glsl
varying highp vec2 vTextureCoord;
varying highp vec3 vLighting;
  
uniform sampler2D uSampler;
  
void main(void) {
  highp vec4 texelColor = texture2D(uSampler, vec2(vTextureCoord.s, vTextureCoord.t));
    
  gl_FragColor = vec4(texelColor.rgb * vLighting, texelColor.a);
}
```

---

## In the shaders...

The vertex shader 

* Set the `vLighting` depending on which way it is pointing and where the lighting is coming from

The fragment shader

* Picked the corresponding point in a texture, and adjusted it by the `vLighting` value that was set on the triangle's first vertex


---

## Compiling the shaders

```js
var shaderScript = document.getElementById(id);
```

```js
  if (shaderScript.type == "x-shader/x-fragment") {
    shader = gl.createShader(gl.FRAGMENT_SHADER);
  } else if (shaderScript.type == "x-shader/x-vertex") {
    shader = gl.createShader(gl.VERTEX_SHADER);
  } else {
    return null;  // Unknown shader type
  }
  
  // Send the source to the shader object
  gl.shaderSource(shader, theSource);
  
  // Compile the shader program
  gl.compileShader(shader);
  
  return shader;
```

---

## But then we have to link them!

```js
  shaderProgram = gl.createProgram();
  gl.attachShader(shaderProgram, vertexShader);
  gl.attachShader(shaderProgram, fragmentShader);
  gl.linkProgram(shaderProgram);
  
  gl.useProgram(shaderProgram);
```

---

## Drawing...

* Now to draw the scene, we need to set the active shaders and geometry, and draw the scene


---

## Setting the active texture and shaders

```js
// Specify the texture to map onto the faces.  
gl.activeTexture(gl.TEXTURE0);
gl.bindTexture(gl.TEXTURE_2D, cubeTexture);
gl.uniform1i(gl.getUniformLocation(shaderProgram, "uSampler"), 0);
```

---

## Drawing the geometry

```js
// Draw the cube.
gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVerticesIndexBuffer);
setMatrixUniforms();
gl.drawElements(gl.TRIANGLES, 36, gl.UNSIGNED_SHORT, 0);
```

---

## Summary

1. Get a `<Canvas>`
2. Get the WebGL context from the canvas
3. (Some set-up stuff, eg what colour "clear" is)
4. Get the text of your shader scripts (normally from a `<script>` tag)
5. Ask the browser to compile and upload your shader scripts
6. Upload your geometry (vertices) and texture data in "buffers"
7. Set the active texture and shaders
8. Ask the graphics card to draw your geometry  




---

## A gotcha...

* I haven't said *anything* about **text** ... because WebGL doesn't contain any methods for rendering text!

* But you can work around it...

  - if the text is flat to the screen, just superimpose a Canvas or HTML over the top!

  - if the text needs to be rendered onto the geometry (eg, the rotating cube), you can render the text into a texture.

---

## Rendering text to a texture

1. create another (hidden) Canvas.  
2. Paint the text to the hidden canvas with the 2D canvas API.  
3. Use `gl.texImage2D(...)` to paint the hidden canvas into a texture

---

## The text rendered to a hidden canvas

```js
var canvas = document.createElement('canvas');
```
```js
var cubeImage = document.getElementById('hiddenCanvas');
var ctx = cubeImage.getContext('2d');
ctx.beginPath();
ctx.fillText(text, ctx.canvas.width / 2, ctx.canvas.height / 2);
ctx.restore(); 
```

---

## Making a texture from the canvas

```js
gl.bindTexture(gl.TEXTURE_2D, texture);
gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
gl.generateMipmap(gl.TEXTURE_2D);
gl.bindTexture(gl.TEXTURE_2D, null);
```








