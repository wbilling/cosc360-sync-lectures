## Assignment 1 / Web development project

By now, you've been given a small amount of base code to build on. But the project you decide to build with this is up to you.

The primary requirements are technical. Your project should

* Be a web application built on top of the Play Framework
* Send JSON events to the client over WebSockets or Server Sent Events
* On the client-side, be a "Single-Page App" that uses React.js, Angular.js, or d3.js to interact with the user and to update the display in response to events sent from the server over the Websocket or SSE connection
* Store and retrieve some data from MongoDB on the server
* Use canvas, SVG, webGL, or some other visualisation library to show visualisations that update.

As a mission for this year, your project should somehow be on the theme of event-driven data visualisation, and data exploration.

### Design and crit videos

This has been delayed from my original plan

By week 6, I would like you to post a design video. This should explain what your project will do, why that's a useful, interesting, or meaningful project, and how your application is going to work. (Max 5 min)

The following week, you will be allocated three other students' videos to watch and post a reply video, giving them your advice. Your advice should be constructive, specific, actionable, and helpful. (Max 3 min).

Please note, there are **no marks** for the production quality of the video. Pointing a mobile phone camera at paper drawings and talking through them is a valid (and often successful) strategy.


### Marked up request

With your code, I would like you to submit commented examples of three kinds of request from your system. Including either a WebSocket or Server Sent Events request.

Capture the request and response headers. (You can use Firefox developer tools to do this). Paste the text of them into a file called `request_example_1.txt` in the root level of your project. Mark up each line with a comment on what that line of the protocol means. And submit it with your code.

```http
# The first line of the request looks like a standard HTTP request, with method (GET), path (/ws) and protocol version (HTTP/1.1)
GET /ws HTTP/1.1 

# A request header containing the server address as the client understands it (as the server might have multiple domains mapped to the same IP address)
Host: server.example.com
```


### Checkpoint

At week 7, you should have written some code, including some model classes and controllers. If you create a repository on GitLab, and post your code, I am happy to have a look at it and start providing some feedback.
 

## Marking

* Got something working: **5 marks**
    * Some code has been written
    * Compiles, has code, starts, doesn't break
    * Responds to HTTP requests
    * Landing page works

* Event-driven connection: **10 marks**
	* Makes a WebSocket or Server Sent Events connection
	* The server sends the client events over the connection
	* Data from the client can in some way affect what the server sends over the connection

* API: **5 marks**
    * The application has a JSON api
    * The JSON api is meaningful and works
    
* SPA: **10 marks**
   * Renders something using React.js, Angular.js, or d3.js
   * Updates meaningfully from data over the event stream
   * The dynamic part of the page is interactive and can cause events or requests to be sent to the server
   * The dynamic part of the page visualises something using SVG, Canvas, WebGL, or some other graphical means

* MongoDB: **10 marks**
	* Connects to MongoDB database
	* Stores data in MongoDB in the correct database
	* Data still retrieves correctly after stopping and restarting the server

* Design and critique: **30 marks**
   * The project has a meaningful coherent purpose
   * The project is usable, well-designed for its purpose, and reasonably attractive.
   * Whether you have provided constructive, actionable, specific and helpful advice in your critique videos

* Quality: **20 marks**    
   * Code quality, organisation, and readability

* Annotated requests: **10 marks**

