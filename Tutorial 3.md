# Tutorial 3 - the code is out..

This week, I've released a little base code for your project. It has only a few extra classes in it than last week's tutorial getting Play running.

But first let's get the code.

### Getting into GitLab

COSC360/560 now has a group on GitLab, so I'd recommend joining it

Go to [https://gitlab.une.edu.au/groups/cosc360-2016](https://gitlab.une.edu.au/groups/cosc360-2016)

You can login using your UNE account name and password, in the LDAP login box. Request access to the group (there should be a button).

### Forking the code

The starting code is on GitLab at [https://gitlab.une.edu.au/cosc360-2016/project-base](https://gitlab.une.edu.au/cosc360-2016/project-base)

I'm going to suggest you "fork" the code on GitLab -- that way you'll have your own repository that you can commit changes to. (But it's not compulsory -- you can just press the download button and get a zip of the code instead)


### A little practice with gitlab

(if you'd like to use GitLab)

1. Open a shell on turing and run `setproxy` so git knows your proxy authentication details

2. Create a public repository on GitLab (don't worry, we'll delete it later)

3. The page you will be shown for the repository will have instructions on how to "clone" it. Follow those instructions. (It'll be git clonefollowed by a URL for your repository.)

4. cd into the directory that git clone created

5. Create a file in that directory called  test.txt and put some text into it. eg:

  ```
  This is a test repository of no particular interest
  ```

  You have now made a change to your "working directory". 

6. Run:

  ```
  git status
  ```

  See what git thinks the status of your repository is (you have a new file, but have not "added" it to be committed.

7. Run:

  ```
  git add test.txt
  git status
  ```

  See what git thinks the status of your repository is now.

8. Run:

  ```
  git commit -m "Added a test text file"
  git status
  ```

  You have now "committed" your change. Internally, git keeps snapshots of your directory (in a cleverly compressed format). By committing your change, you have created a new snapshot.

9. Run:

  ```
  git log
  ```
  
  You should see the history of your code -- it's possible to go back to earlier versions, but we're not going to do that now.

10. Have a look at your repository on GitLab (in the browser). It hasn't changed...

  You've created a new snapshot on turing, but haven't yet "pushed" it to GitHub. It only exists on turing at the moment.

11. Run:

  ```
  git remote
  ``` 
  
  This lists the remote repositories that your repository knows about -- ones you can push changes to, and fetch or pull changes from. One of the ones in the list should be called "origin" and it is the repository you created on GitHub. (When you clone a repository, the remote repository you cloned is automatically called "origin" in your clone) 

12. Run:

  ```
  git push origin master
  ```
  
  This pushes the branch called "master" to the remote repository called "origin". 

13. Refresh the browser page on GitHub. Your changes should now be there.

You now have a way of moving code between a computer and GitHub. Keep a repository on GitHub, and make a clone of the repository on each computer. Commit your code in logical chunks to make snapshots (eg, when you've done a chunk of work). Push the change to GitHub to share it. And you can pull the changes from another machine using git pull.

You can now delete the public repository you created.

### Getting started with the project code

#### Actors

First, start the server, and visit `/` on the server. You should see the beginnings of a fizz-buzz game, and if you reload the page you'll see more of it.

Next, open the code in IntelliJ, and have a look in `app/actors`. This is where the FizzBuzz players are.

We'll talk through Actors in the videos, but essentially, think of them as butlers that have an in-tray they pick up tasks from, then they go and do the task, and then they look in their in-tray for the next one. So, the main part of an Actor is its `onReceive(Object message)` method.

Try to look through the code, and see if you can work out how it works. What does the Marshall do? Make some changes and break things!

#### WSClient

Next, open `app/controllers/Application.java`.

The first thing you might notice is some annotations -- `@Inject` and `@Singleton`. Play framework includes Google Guice, which is a "dependency injection" framework. In other words, it will try to do some of the set-up for you.

The next thing to have a look at is the `whatDidGitLabSay` method. 


### Thinking about your project...

Ok, having had a look around, you can put the code aside for a moment.

In your project, you're going to build a web-app, with a few requirements

- it needs to bring data into the app from outside (in real-time)
- it needs to somehow visualise that data in the browser, updating it live as new data comes through
- it needs to use the technologies we're teaching in the course. 
- it needs to be interactive

(More details will be posted under the assignment page)

So the next task in the tutorial time is to think about what you would like to build...


