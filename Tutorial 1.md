# Tutorial 1 -- dig, curl, and sbt


## Part 0: Join the GitLab group

Come join the GitLab group, where you can push your code to share with your class:

[https://gitlab.une.edu.au/cosc360-2017](https://gitlab.une.edu.au/cosc360-2017)

## Part 1: Getting through the proxy

1. Connect to turing.une.edu.au, either via nx (NXClient, x2go, etc) or via ssh, and open a terminal

2. Set your proxy settings for unix utilities

    ```
     setproxy 
    ```

   Follow the prompts and this will set the environment variables `http_proxy` and `https_proxy`. That will let commands you execute from that shell call out through the UNE firewall. Well, except that Java based ones (Java doesn't use those environment variables), but more on that later.

## Part 2: DNS exploration with dig

Let's see how Domain Name Service records work. There'll be a video on this after the tutorial, but let's get you to look at it first.

What IP address does `www.une.edu.au` go to?  Let's find out...

```bash
dig www.une.edu.au
```

If we look in the "Answer section" of what happened, we should have received a `CNAME` response. That means "it's the same IP address as the following domain name". So now let's look up that one

```bash
dig une.dr.squiz.net
```

In the answer, we should get another CNAME, but that refers to the `A` record just below it. A is for Apex.

So, now we know UNE outsources their webhosting to Squiz!

Ok, but what about UNE's email?

```bash
dig une.edu.au mx
```

That gives us some `MX` (mail exchange) records, and if we look further down in the answer, those don't go to Squiz.


## Part 2: HTTP exploration with curl

We'll begin by issuing a few HTTP requests via curl, to get used to it and make sure it works.

When you make each request, read the output, and see if you can work out what each HTTP header in the request and response means.

   
3. Let's do a simple curl to Google and follow the redirect manually

    ``` 
    curl -v http://google.com
    ```
   
   This should result in a `302 Moved Temporarily` response. Take note of the URL it redirects to, and make a GET request to that URL. What is the length of the response?
   
4. The `-L` flag will cause curl to automatically follow redirect responses. Let's try it

    ``` 
    curl -v -L http://google.com
    ```
 
    Was the response length the same as before?
    
    Did the second request use the same HTTP version? If not, what do you think happened?
   
      
4. Let's search for COSC360 using DuckDuckGo

   ```
   curl -v https://duckduckgo.com/search?q=COSC360
   ```
   
   Which open source web server does DuckDuckGo use?  
  
5. Let's try an API request to GitHub

    ```
    curl -v https://api.github.com/users/octocat
    ```
    
   What is the MIME type (Content-Type) for JSON data?
   
6. Let's try setting a request header

   ```
   curl -v -H "Accept: text/plain" https://api.github.com/users/octocat
   ```
   
   The Content-Type of the returned data doesn't match what we put in the Accept header. Why is that?
    

## Part 3: Content Distribution Networks

Let's start this part with a question. Latency is the thing that makes the Web slow. Suppose you're writing an app in Armidale, and it's being used by someone in Tenerife (Canary Islands, roughly antipodal to Australia). What is the shortest latency that is physcially possible?

[Here's a theoretical answer](https://www.wolframalpha.com/input/?i=%28circumference+of+the+earth+%2F+2%29+%2F+%28speed+of+light+%2F+1.5%29)

If you're wondering what the 1.5 is, that's the refractive index of glass (how much slower light travels in an optical fibre).

Oh, but that's just the time for the request to reach the server. For the reply to come back, it's going to take that long again. So double that number (and we get ~200ms -- a fifth of a second just for one request).

Now the tricky question: **How can we make the latency appear shorter?**


### CDNs -- an answer to the tricky question

In this unit, we're going to be developing live interactive sites. But there's quite a lot of the *content* of a site that is *fixed* resources. The images don't change. The JavaScript, though we write it and it's interactive when it runs, it is a file full of text. So we could *cache* that in a server that is physically closer to the user.

So, one way that companies try to take load off their servers, and reduce latency for their customers, is to use a *Content Distribution Network* (CDN). Instead of sending the request directly to the server we're writing, the web request goes to the CDN. And if it's just asking for static content, it's sent to a cache near the user, and if it's not it gets sent through to your server.

Take a look at the explanation by one of the most popular commercial CDNs:   
[CloudFlare](https://support.cloudflare.com/hc/en-us/articles/205177068-Step-1-How-does-Cloudflare-work-)


### www and Naked domains

So, to make the Web requests go through CloudFlare, they ask you to point a DNS record CNAME at one of their servers.

But they ask you to point a *subdomain* (eg, `www.example.com`) not an *apex* domain (eg, `example.com`). What if someone makes a request to `example.com`? Well, we'd really like to redirect them to `www.example.com`...

Try this to see how to solve this problem:  

```
curl -v wbillingsley.com/veautiful
```


## Part 4: Thinking about your project

You've got two big decisions to make fairly early:

1. What are you going to build?
2. What server-side framework are you going to build it in?

Let's spend the rest of the tutorial thinking about those questions.

### Your project

So, remember, as well as the technical requirements, the app you build will have to need:

* Some kind of real-time communication between the client and the server
* Something graphical to show, interact with, and update in the browser

And not just be a chat app.

If you're totally stuck for ideas, I usually recommend doing some kind of multi-player game. Because it's easy to bend those to what you want them to be -- they just have to be fun and well-made.

But otherwise, there's any number of ideas you could do. Collaborative drawing? Interactive mathematics? 


### Choosing a framework

This is harder to give advice on. There's three languages you can use:

* JavaScript (Sails.js)
* Java (Play or Spring MVC)
* Scala (Play)

If you don't have a favourite language, you can think about what your view is on compiler errors: something that picks up your bugs before you run it (a help), or a frustration because the compiler makes pressing "reload" to recompile your code take a few seconds longer?

All the frameworks have ways of working so you can click refresh in the browser, and your code changes will come through. But as Scala and Java need to be compiled, this can take a few seconds with Play.

