class: center, middle

# Routes, Patterns and Partial Functions

.byline[ *Will Billingsley, CC-BY* ]

---

## First...

* Watch the Typesafe intro to Play Framework

--

* This video is just giving you a bit of a look at the insides...

---

### Action not found...

You've seen the `routes` file, but what does it compile to?

```
# Home page
GET     /                           controllers.Application.index()
GET     /upper                      controllers.Application.upper(input)
GET     /form                       controllers.CaptchaController.simpleForm(query ?= null)

# Add routes for the beagles here...

# Map static resources from the /public folder to the /assets URL path
GET     /assets/*file               controllers.Assets.versioned(path="/public", file: Asset)
```

---

### routes.scala

* Compiles to a Scala file in target/scala-2.11/routes/main/router

* (Sometimes exploring the generated file will help you get package names right -- eg reverse routes)

---

```
  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:6
    case controllers_Application_index0_route(params) =>
      call { 
        controllers_Application_index0_invoker.call(Application_2.index())
      }
  
    // @LINE:7
    case controllers_Application_upper1_route(params) =>
      call(params.fromQuery[String]("input", None)) { (input) =>
        controllers_Application_upper1_invoker.call(Application_2.upper(input))
      }
  
    // @LINE:8
    case controllers_CaptchaController_simpleForm2_route(params) =>
      call(params.fromQuery[String]("query", Some(null))) { (query) =>
        controllers_CaptchaController_simpleForm2_invoker.call(CaptchaController_0.simpleForm(query))
      }
  
    // @LINE:13
    case controllers_Assets_versioned3_route(params) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned3_invoker.call(Assets_1.versioned(path, file))
      }
  }
```

--- 

## Scala...

* A "mixed paradigm" language that runs on the JVM

--

* Internally, Play is written in Scala

-- 

* There is a Scala API for Play   
  (yes you can write your app in Scala if you would like)

---

## Scala...

Suppose we wanted a function that said what we were doing on a particular day

```scala
def whatAmIDoingOn(day:Day):String { 
  if (day == Monday || day == Thursday) { 
    "Learning to build great Web apps!"
  }
}
```

That doesn't compile. Why not?

---

## Functions

* $$sin(\pi/2) = 1$$

--

* What is *log(0)*?

--

  Let's [Google it...](https://www.google.com.au?q=log+0)

--

### Partial and total functions

* A **total function** is defined over every element in its domain

* A **partial function** is only defined for part of its domain

---

## Partial functions

* In Scala, we can also define partial functions

--

    ```scala
    val whichPrime:PartialFunction[Int, Int] = {
      case 2 => 1
      case 3 => 2
      case 5 => 3
      case 7 => 4
    }
    whichPrime(3)             // 2
    whichPrime.isDefinedAt(4) // false
    ```

---

## Pattern matching

* Scala also has built in support for pattern matching

--

```scala
def find(s:List[String]) = s match {
  case "my" :: x :: Nil => x
}

find(List("my", "pal"))  // pal
find(List("your", "pal"))  // Match Error
```

