# Tutorial 8 - Fetch

In this tutorial, we're going to tie the front-end to a simple back-end.

We'll use the Vue client as our example, and there are three different back-ends we might want to connect it to:

* Express for TypeScript
* Play for Scala
* Play for Java

Which you should pick mostly depends on which programming language you feel most confident with. (TypeScript, Java, or Scala.) 

## 1. Clone the back-end

I've created three starter-projects and published them to GitLab. Let's give you the link to the git repository for each. They each have a README.md to explain what's going on in each.

1. **Express for TypeScript**  
   [https://gitlab.une.edu.au/cosc360in2018/express-typescript-seed](https://gitlab.une.edu.au/cosc360in2018/express-typescript-seed)  

   Set up with an MVC-like directory structure and TypeScript

2. **Play for Java**  
   [https://gitlab.une.edu.au/cosc360in2018/play-java-seed](https://gitlab.une.edu.au/cosc360in2018/play-java-seed)

   Play projects can be edited using IntelliJ IDEA Community Edition with the Scala plugin (see section below for setup details). Choose File -> Open... and select the `build.sbt` file and then select to open it as a project.

2. **Play for Scala**  
   [https://gitlab.une.edu.au/cosc360in2018/play-scala-seed](https://gitlab.une.edu.au/cosc360in2018/play-scala-seed)
   
   Play projects can be edited using IntelliJ IDEA Community Edition with the Scala plugin (see section below for setup details). Choose File -> Open... and select the `build.sbt` file and then select to open it as a project.

**Note for Play projects**: If you are working on turing and using a Play project, there's an extra step you're going to have to do. `sbt` is a launcher script that will try to fetch sbt proper from the internet. But it won't get through the web proxy, and it won't yet have read the build file to know there's a local repository it can get it from. So, from one of the Play seed projects' top-level directories:

```sh
sbt -Dsbt.repository.config=repositories
```

This will cause the sbt launcher script to use Hopper to retrieve sbt. You should only need to do this once, as then it will have fetched (and cached) sbt for you. You can exit the sbt shell with Ctrl-d


### Working with IntelliJ on Turing

IntelliJ is installed on turing and can be run by running `idea.sh` from a command prompt.

However, we need to install the Scala plugin, and before we can do that, if we're working on turing we need to tell IntelliJ how to get through the UNE web proxy.

Choose Preferences... and type "proxy" in the search box. Choose Manual Configuration, and put in:

* Host name: `proxy.une.edu.au`
* Port number: `8080`
* No proxy for: `localhost, *.une.edu.au`
* Tick proxy authentication, and enter your username (not email address) and password.

Click Ok. Reopen preferences and search for "plugin". Select "Install JetBrains plugin" and search for "Scala" and install the Scala plugin.

IntelliJ Ultimate Edition (which you can get a free license for) includes some more advanced features for working with Play in IntelliJ, but the Scala plugin is all that's needed for basic editing.

To open a Play project in IntelliJ, File -> Open... and select its uppermost `build.sbt`. IntelliJ should then ask whether you want to open this as a file or a project. Choose project, and the defaults in the following dialog are usually ok.

### Explore a little

Now that you have the backend up, explore a little.

If on turing, choose a port using:

```sh
my_tomcat_ports
```

and start the server on that port number. Then try:

1. Making a GET request using `curl -v` to the `/example/json` route

2. Making a POST request using your favourite REST client (eg, Postman or RESTclient) to `/example/json`

3. Altering what the server actions return -- make it return a JSON array of 

## 2. Make the server serve the client

So far we've built our front-end using Webpack. Our front-end is compiled into static files, so for this unit we can just continue to do so.

Although it is possible to wire up the server to have tasks to rebuild the front-end, you will probably find it simpler for now just to put the front-end project into the public/static files area of the back-end, and continue using webpack to rebuild it. There's not long left in trimester, so let's not overcomplicate things.

cd into the public directory of your server. 

* For Play (Java or Scala), this is `public`
* For the Express seed project, I have set it up to be `build/public`. **Beware of any actions that might clean out the build directory!**  

Clone the client using:

```sh
git clone git@gitlab.une.edu.au:cosc360in2018/tutorial-conway-life-t6-front.git
```

or

```sh
git clone https://gitlab.une.edu.au/cosc360in2018/tutorial-conway-life-t6-front.git
```

and then rename the resulting directory `client`

Alternatively, you might wish to use a symbolic link:

1. clone the client into an entirely separate directory

2. in the public directory of the server, symlink the client with  
   `ln -s path_to_client client`  
   (replacing *path_to_client* with the path to where you cloned the client code)

Start the server, and in your browser on the relevant port visit `public/client/index.html`. The client should load.

The client is currently set to use the Vue solution to tutorial 5. If you prefer react, you could checkout the react-solution branch of the client.

## 3. Today's challenge

Today's challenge is going to ask you to make a few small connections between the server and the client.

1. Make `/example/game` visit the game, with an id

   In the seed projects, the `/example/game` route is set up so that it will allocate an `id` parameter if there isn't one. And it then just redirects to itself and returns some text. In the Play seed projects, this is in `app/controllers/ExamplesController`. In the Express seed project, it is in `app/controllers/welcome.controller.ts`

   Once it has an `id` parameter, make it instead redirect to the url of the game client *but with the `id` query parameter still present*.

2. Create `GET` a route in the server, `/example/gamedata` that will take query parameters for a game id and a length, and will then return an array of random booleans of that length. Use the existing controller actions as examples, and don't forget to edit the routes (in Play, in `conf/routes`; in Express seed in `server.ts`) to point the route to your controller action. Test it from curl.

3. Alter the client code to extract the `id` from the location. In the browser, you can get the query string out of the location from  

   `let id = (new URL(window.location.toString())).searchParams.get("id")`

   First, just get it and log it to the console to see that you're getting it ok.

4. Alter the client so that when you press start, it queries your `/example/gamedata` route to get data to initialise the game.

5. Alter your `/example/gamedata` route on the server so that when it issues an array for a game id, it also stores it in a Map held on the Controller. When a request for game data comes in, it should first see if there already is data in the map (if so, send it) and otherwise generate and store data to send.
