
class HelloMessage extends React.Component {
    render() { 
        return <div>Hello {this.props.name}</div>;
    }
}

let mountNode = document.getElementById("renderhere")
let theName = "Bertie"

ReactDOM.render(<HelloMessage name={theName} />, mountNode);
