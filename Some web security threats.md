class: center, middle

# Some web security threats

---

### Injection attacks

* These are when a bug allows users to enter data that is misinterpreted as code

* Most common is SQL Injection attack

* Usually caused by inserting a user-given parameter into an SQL query inappropriately

* **Number 1** on OWASP Top 10

---

### SQL Injection attack example

<iframe src="sqlinjection.html"
  style="border: none; width: 90%; height: 500px;"
></iframe>


---

### Session hijacking

* Your session ID is usually stored in a cookie. eg:

```http
Cookie: PLAY_SESSION="c6e5c5e39ddbb4a5c94e9e207462490ef91064ba-sessionKey=57094f9e-c998-4232-88a5-ed3632b51c0f
```
  
* If I can steal your cookie, the server will think I'm you

---

### Not logged in, a request to my api should fail

```bash
$ curl -d "" http://localhost:9000/api/self
Not found
```

---

### Let's sniff a request from my browser

```http
POST /api/self HTTP/1.1
Host: localhost:9000
Connection: keep-alive
Content-Length: 0
Cache-Control: max-age=0
Accept: application/json
Origin: http://localhost:9000
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36
Content-Type: text/plain; charset=UTF-8
Referer: http://localhost:9000/
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.8
Cookie: PLAY_SESSION="c6e5c5e39ddbb4a5c94e9e207462490ef91064ba-sessionKey=57094f9e-c998-4232-88a5-ed3632b51c0f"
```

---

### Let's make a request with my cookie

```bash
$ curl -d "" -H "Cookie: PLAY_SESSION=\"c6e5c5e39ddbb4a5c94e9e207462490ef91064ba-sessionKey=57094f9e-c998-4232-88a5-ed3632b51c0f\"" http://localhost:9000/api/self
```
```
{"error":null}
```

Ok, that didn't work -- the user wasn't logged in

---

### Now let's retry the request when the user happens to be logged in

```bash
$ curl -d "" -H "Cookie: PLAY_SESSION=\"c6e5c5e39ddbb4a5c94e9e207462490ef91064ba-sessionKey=57094f9e-c998-4232-88a5-ed3632b51c0f\"" http://localhost:9000/api/self
```
```
{"id":"55e42fac5427cf58789a363a","pwlogin":{"pwhash":["$2a$10$1GZwb5D.F1DKfthXxpyD9ePRmnh9EMHYnj4ewVJyn.AUdVxOXZ7Kq"],"email":["admin@comp284"]},"secret":"J90A7HqvtijkNl4G","activeSessions":[{"key":"byhand","ip":"127.0.0.1","since":"1441017772466"},{"key":"57094f9e-c998-4232-88a5-ed3632b51c0f","ip":"0:0:0:0:0:0:0:1","since":"1441850813633"}],"created":"1441017772485"}
```

Yikes! Just as well that's a disposable account running on localhost (my laptop) not anything real

---

### Cross-Site Scripting

* Effectively, an injection attack *in the browser*

* Happens when user input is composed into HTML that is displayed on the page

* Could cause scripts to be run, or actions to be made

---

### Handy example: the page I wrote for th SQL Injection demo

```js
<script>
var dosearch = function() {
  console.log("donnit")
  $("#output").html($("#search").val())
}
</script>
```

* That JQuery call is taking the contents of an input element, and composing it into the page

* Intention was to show what happens if we insert dodgy SQL into the string, but what happens if we insert a script...

---

### Handy example: the page I wrote for th SQL Injection demo


<iframe src="sqlinjection.html"
  style="border: none; width: 90%; height: 500px;"
></iframe>
