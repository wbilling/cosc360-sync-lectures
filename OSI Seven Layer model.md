class: center, middle

## Advanced Web Programming
(COSC360/570)

---

## Network Effects

The first phonebook?

--

```
1. Bell, Alexander Graham
2. Watson, Thomas A.
```

--

![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/10/Alexander_Graham_Bell.jpg/220px-Alexander_Graham_Bell.jpg) 
![](https://upload.wikimedia.org/wikipedia/en/thumb/f/f8/Thomas_watson.jpg/200px-Thomas_watson.jpg)

---

### Metcalfe's Law

> The value of a network is proportional to the square of the number of users

Applies to telephones, internet, Web, social networks...

--

### A consequence

A high value network is *very hard to displace*

--

How hard would it be to displace Facebook?


---

![](cosc360/1/instagram headline.png)

* In 2012, Facebook bought Instagram (photo-sharing app) for $1bn  

--

* Before 2012, Facebook was mostly used on desktop computers.
  
--

* Instagram added 130,000 new users per week on mobile (Mar 2011).

--

* Fear that move to mobile would displace their market?

---

### Network effects and the Web

--

* HTTP is an old technology. 1.1 released in 1999. *Only just replaced!*

  Designed for *hypertext*

--

* JavaScript is an *old* technology, designed for *little animations*

--

* But everyone's browser talks HTTP, and everyone's browser has JavaScript

--

* So everyone's server talks HTTP, and everyone's server serves up JavaScript

--

* ***Two sided network effect***

---

### TLDR;

* We're trying to build modern, interactive, real-time applications...

* ...on a platform that was fundamentally designed for 1990-era static hypertext

--

> **expect a certain amount of awkwardness!**

---

### The good news

* It is probably the most inspectable programming environment you will encounter. Everything is text, and you already have some debugging tools...

--

* *Abstractions are your friend*. Small things working together.

---

class: center, middle

# OSI Seven layer network model
(An example of abstraction)

---

---

### Networks are complex, so...

* *Application layer* - at this layer, we offer services to applications. HTTP sits at this layer.

* *Presentation layer* - this deals with translation issues if we want to represent the same data differently (eg, character sets)
TLS operates at this layer (encrypting the data is changing its presentation)

* *Session layer* - sessions are logical connections (conversations). 

  TLS (also called SSL) is initiated at this layer -- when the session starts, negotiate key exchange to encrypt the data 

* *Transport layer* - we don't just want to get a single packet through, we want to deal with Quality of Service issues
TCP is a reliable protocol - if a packet is lost, it ensures it is resent
UDP is an unreliable fire-and-forget protocol (doesn't resend lost datagrams)

* *Network layer* - given that we can have many networks connected together, how can my computer on my WiFi network address a computer on in a data centre's network on the other side of the world? Internet Protocol (IP) and IP addressesare at this layer.

* *Data link layer* - how the linked computers can form a single network, eg. MAC addresses so the network can send a device some data

* *Physical layer* - how computers send bits to each other
Layers do not (generally) need to know what is happening at the layers beneath them.

So, HTTPS (HTTP over TLS -- with data encrypted between the client and the server) looks to the application code as if it is just HTTP.




---

class: center, middle

# Domain Name Service (DNS)


---

### Domain Name Service

* Designed in 1983. This is what the "cool" home computers looked like in 1983 (but not the servers that supported DNS):

  ![](https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcQHLf--q3xwdGjX7qbUIzbJD4OEcObl_pVkVLMfWw_WAC1yJzic)

--

* Documented in [RFC1034](https://tools.ietf.org/html/rfc1034) and 
  [RFC1035](https://tools.ietf.org/html/rfc1035) 

    **Handy hint:** Web protocols are usually documented in plain-text RFCs. Sometimes they are a better reference than a textbook...

---

### Domain Name Service

*And sometimes RFCs contain ASCII-art...*

```
                 Local Host                        |  Foreign
                                                   |
    +---------+               +----------+         |  +--------+
    |         | user queries  |          |queries  |  |        |
    |  User   |-------------->|          |---------|->|Foreign |
    | Program |               | Resolver |         |  |  Name  |
    |         |<--------------|          |<--------|--| Server |
    |         | user responses|          |responses|  |        |
    +---------+               +----------+         |  +--------+
                                |     A            |
                cache additions |     | references |
                                V     |            |
                              +----------+         |
                              |  cache   |         |
                              +----------+         |
```

Source: [RFC1035](https://tools.ietf.org/html/rfc1035) 

---

### A, CNAME, and MX

* Text mapping from Domain Names to IP addresses for different message types. 

    ```dns
    USC-ISIC.ARPA   IN      CNAME   C.ISI.EDU

    C.ISI.EDU       IN      A       10.0.0.52  
    ```

---

### Domain Name Service

* Most important records are *A*, *CNAME*, and *MX*

    ```
    A               For the IN class, a 32 bit IP address

                    For the CH class, a domain name followed
                    by a 16 bit octal Chaos address.

    CNAME           a domain name.

    MX              a 16 bit preference value (lower is
                    better) followed by a host name willing
                    to act as a mail exchange for the owner
                    domain.
    ```

    Source: [RFC1034](https://tools.ietf.org/html/rfc1034)
    


---

### Domain Name Service

* Text mapping from Domain Names to IP addresses for different message types. 

    ```dns
    USC-ISIC.ARPA   IN      CNAME   C.ISI.EDU

    C.ISI.EDU       IN      A       10.0.0.52  
    ```

---

### `dig`

```bash
; <<>> DiG 9.8.3-P1 <<>> une.edu.au
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 60128
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 0

;; QUESTION SECTION:
;une.edu.au.			IN	A

;; ANSWER SECTION:
une.edu.au.		299	IN	A	202.9.95.188

;; Query time: 311 msec
;; SERVER: 10.0.1.1#53(10.0.1.1)
;; WHEN: Mon Jun 27 11:26:11 2016
;; MSG SIZE  rcvd: 44

```

---


