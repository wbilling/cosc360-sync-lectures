class: center, middle

# EventSource

---

## Overview

* A **WebSocket** is a *binary*, *bidirectional* channel between the client and the server

--

* An **EventSource** is a *text*, *unidirectional* channel from the server to the client.  
  Also known as "Server-Sent Events"

--

* EventSource also handles connection drops for you -- whereas with WebSocket if you lose the connection for any reason you need to handle that

--

* Not yet supported in Microsoft Edge

---

## Client-side

Client-side code looks very similar to working with WebSockets

```js
let sse = new EventSource("/eventsource?topic=Algernon");

sse.onmessage = (msg) => {
    let json = JSON.parse(msg.data)

    myJsonArray.push(json)
    rerender()
}
```

---

## EventSource request

An EventSource is an ordinary GET request with a *chunked* response. This lets the server stream messages whenever it wants as new chunks.

The MIME type for this is `text/event-stream`

```http
GET /chat HTTP/1.1
Host: server.example.com
Accept: text/event-stream
```

---

## EventSource response

Because the response is chunked, there is no `Content-Length`

```http
HTTP/1.1 200 OK
Connection: keep-alive
Content-Type: text/event-stream
Transfer-Encoding: chunked

retry: 15000

data: Here is a message

data: Here is another message
```

---

## Chunked Response

* Behind the scenes, chunked responses send chunk-lengths followed by the text of the chunk. But the HTTP handler deals with that. What you see are events separated by blank lines.

eg: 

```
data: Here's my message \n\n
```

---

## Mutliline messages

A blank line ends a messages, so this is a single message with an old joke:

```
data: How many academics does it take to change a lightbulb?
data: Change?
```

---

## JSON messages

* Event Source sends text messages, but it's easy enough to serialise JSON as text

```
data: {
data:   "setUp": "How many academics does it take to change a lightbulb?"
data:   "punchline": "change"
data: }
```

---

## Pros and Cons

* EventSource is very simple. Unidirectional, text. And you can fill in for the lack of client->server messages just by making ordinary POST requests.

* But it's not as widely supported -- particularly, IE and Edge do not yet support EventSource

---

### Play for Scala

Slightly more compact than websockets, because we have no events coming in from the browser

```scala
def eventSource() = Action { request =>
    // The source "will be" a queue we can push to. 
    val s = Source.queue[String](50, OverflowStrategy.backpressure)

    // Connect the source via the EventSource's flow.
    val stream = s.viaMat(EventSource.flow[String]) {
      // It calls us back when the queue has been created
      case (queue, m) =>
        listeners.add(queue)
        m
    }

    Ok.chunked(stream).as("text/event-stream")
  }
```

---

### Play for Java

```java
public Result eventSource() {

    // An eventsource is a chunked response
    return ok().chunked(
        // We're going to use an asynchronous queue as our source
        Source.<String>queue(50, OverflowStrategy.backpressure())

            // We'll need to pipe it through a function to wrap each string as an event
            .map((str) -> EventSource.Event.event(str))

            // And then we'll need to "materialise" the source
            .viaMat(
                // EventSource provides the sink 
                EventSource.flow(),

                // This callback lets us get the queue we can offer messages on
                (queue, done) -> {
                    queues.add(queue);
                    return done;
                }
            )
    ).as("text/event-stream");
}
```

