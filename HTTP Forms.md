class: center, middle

## Forms
(and starting to look at specific headers)

---

### Simple example

```html
<h1>Encrypt a password:</h1>
<form action="encrypt">
    <input type="password" name="pw" />
    <button type="submit">Submit</button>
</form>
```

--

```http
GET /encrypt?pw=hellothere HTTP/1.1
Host: localhost:9000
Connection: keep-alive
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36
Referer: http://localhost:9000/
Accept-Encoding: gzip, deflate, sdch
Accept-Language: en-US,en;q=0.8
```

---

```http
GET /encrypt?pw=hellothere HTTP/1.1
Host: localhost:9000
Connection: keep-alive
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36
Referer: http://localhost:9000/
Accept-Encoding: gzip, deflate, sdch
Accept-Language: en-US,en;q=0.8
```

* Default method is GET
* With a GET request, input names and values are in the *query* in the URL

    `/encrypt?pw=hellothere`      
    `/path?variable=value&another+variable=another+value`
    
---

### URL encoding    
    
* URL encoding:

    `+` stands in for space      
    characters can be % encoded. eg, `%20` is also space
    
* Browser will typically encode the URL shown in the URL bar too, so a link to

    ```url
    http://example.com/?looks=innocent                   &dodgy+stuff+off-screen
    ```
    
    becomes
    
    ```url
    http://example.com/?looks=innocent%20%20%20%20%20%20%20%20%20%20%20%20dodgy+stuff+off-screen
    ```

---

### Connection header

* Whether the TCP/IP connection should be kept open for the next request

  ```http
  Connection: keep-alive
  ```
 
  (As opposed to `Connection: close`)

---

### Accept header
  
* Lists MIME types (content types) that the browser will accept in the response.

  ```http
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/
```
  
  MIME types have the format  
  `top-level type name / subtype name [ ; parameters ]`  
  eg: `text/plain; charset=UTF8`
  
* q is a preference parameter. From RFC2616:

    ```http
    Accept: text/plain; q=0.5, text/html, text/x-dvi; q=0.8, text/x-c
    ```

	*Verbally, this would be interpreted as "text/html and text/x-c are the preferred media types, but if they do not exist, then send the text/x-dvi entity, and if that does not exist, send the text/plain entity."*

---

### User-Agent 
  
* Tells the server what client software is making the request

  ```http
  User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/
  ```
    
  This is often spoofed -- browsers used to have different rendering behaviour, and when releasing a new version, browser manufacturers would adjust the User-Agent string to get the server-behaviour they wanted.

---

### Referer

* What URL did this request *come from*? (for links as well as forms)  

  ```http
  Referer: http://localhost:9000/
  ```
   
* This can leak information -- the destination receives the full URL (including query) of your previous page

---

### Controlling Referer behaviour

* You can control whether Referer is sent by putting a meta element into the page

  ```html
  <meta name="referrer" content="no-referrer">
  ```
  
  Yes, note the different spelling!
   
    - `no-referrer`:
       Don't send it
    - `origin`:
       Just the host. eg, a link from `http://example.com/page.html` would just send `http://example.com/`
    - `no-referrer-when-downgrade`:
       Full URL if the protocol is https (even to a different server), but
       host info only if following a link from an https page to an http URL
    - `origin-when-cross-origin`:
       Full URL for same protocol on same server. Host only for requests to a different server or protocol 
    - `unsafe-url`:
       Yes, this is unsage, including passing full URL from an https page to an http link

---

### Accept-Encoding

*  Tells the server that the client can handle responses that have been compressed using any of these encodings
  
 ```http
 Accept-Encoding: gzip, deflate, sdch
 ```

  
--
  
### Accept-Language

* Just as there may be different formats of a resource, there could be the same resource available in multiple languages. The client includes this header to state its language preference.

 ```http
 Accept-Language: en-US,en;q=0.8
 ```
  
   
---
   
### Handling GET requests in Play   
   
* Can put *expected* request parameters into the controller method's arguments

  In controller:
  ```java
  public static Result matches(String pw) { ... }
  ```
  
  In Routes:
  ```routes
  GET /matches   controllers.Application.matches(pw)
  ``` 
  
  This will give a `Bad Request` response if the parameters are not present
  
  ```http
  HTTP/1.1 400 Bad Request
  Content-Type: text/html; charset=utf-8
  Date: Sun, 19 Jul 2015 23:41:56 GMT
  Content-Length: 2140

  etc
  ```

---

### Defaulting parameters

* Can put a default value for an expected parameter in the route

   ```routes
   GET  /encrypt  controllers.Application.encrypt(pw ?= "hi")
   ```
   
---
   
### Getting parameters from `request` in Scala

* I said earlier that Play was built on a model where an Action is a function from request to response

* This is particularly true for the Scala version of Play

   ```scala
   def method(expectedArg:String) = Action { request => 
     val optionalArg = request.queryString("optional")
     ok("my response")
   }
   ```
   
* But to adapt the API for Java, and make the syntax friendly for Java developers, they did something different

---

### Getting parameters from `request()` in Java

```java
public static Result method(String expected) {
   String[] values = request().queryString().get("optional");
   return ok("This is my response");
}
```

* This looks odd - `method` is static, and called concurrently, but the request is unique for each request...
    
* `request()` gets the current request from a *ThreadLocal* variable -- this is a Java feature that lets you hold a different value for the same variable in each thread. 

  Brief intro to ThreadLocal:  
  [http://www.appneta.com/blog/introduction-to-javas-threadlocal-storage/](http://www.appneta.com/blog/introduction-to-javas-threadlocal-storage/)

  Play framework handles the context, so that you will always receive the correct request from `request()`.
  
---
  
### Request variables can have multiple values

```java
public static Result method(String expected) {
   String[] values = request().queryString().get("optional");
   return ok("This is my response");
}
```

* Notice that `request().queryString().get("optional")` returns an *array* of Strings. That's because in a URL you could have

   ```url
   http://example.com/q?a=1&a=2&a=3&a=4
   ```
   
* But usually you will have one value for an argument, and there is a helper method that just returns one value

   ```java
   String value = request().getQueryString("optional");
   ```
   
---

### Let's make the form a POST

```html
<h1>Encrypt a password:</h1>
<form action="encrypt" method="POST">
    <input type="password" name="pw" />
    <input type="text" name="another variable" />
    <button type="submit">Submit</button>
</form>
```

--

```http
POST /encrypt HTTP/1.1
Host: localhost:9000
Connection: keep-alive
Content-Length: 33
Cache-Control: max-age=0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Origin: null
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36
Content-Type: application/x-www-form-urlencoded
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.8

pw=sdfsdf&another+variable=sdfsdf
```

---

### Headers to notice

* There's no `Referer`. That's because I put in 

  ```html
  <meta name="referrer" content="no-referrer">
  ```
  
* We now have content in our request

  ```http
  Content-Length: 33
  Content-Type: application/x-www-form-urlencoded
  ```
  
* The content is URL encoded by default
  
  ```http
  pw=sdfsdf&another+variable=sdfsdf
  ```

---

### Let's change the encoding

```html
<h1>Encrypt a password:</h1>
<form action="encrypt" method="POST" enctype="multipart/form-data">
    <input type="password" name="pw" />
    <input type="text" name="another variable" />
    <button type="submit">Submit</button>
</form>
```

---

### Multipart/form-data

```http
POST /encrypt HTTP/1.1
Host: localhost:9000
Connection: keep-alive
Content-Length: 246
Cache-Control: max-age=0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Origin: null
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36
Content-Type: multipart/form-data; boundary=----WebKitFormBoundaryATJzgiLVDqEXLEss
Accept-Encoding: gzip, deflate
Accept-Language: en-US,en;q=0.8

------WebKitFormBoundaryATJzgiLVDqEXLEss
Content-Disposition: form-data; name="pw"

aaaaa
------WebKitFormBoundaryATJzgiLVDqEXLEss
Content-Disposition: form-data; name="another variable"

bbbbb
------WebKitFormBoundaryATJzgiLVDqEXLEss--

```

---

### Multipart/form-data

* Separated by a *boundary* 
* First line of each is `Content-Disposition` containing that it is form-data and the argument's name
* Followed by a blank line, and then the content
* This encoding is necessary if any of your input elements are file inputs!

---

### Handling forms at the server...



