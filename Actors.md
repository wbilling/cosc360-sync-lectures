class: center, middle

# Actors 

.byline[ *Will Billingsley, CC-BY* ]

---

## Actor model

Actors are a very simple way to think about concurrency

--

* An Actor can do one thing at a time

--

* When it's done, it goes back to its mailbox to see if it's received any messages. It takes the next message from the queue.

--

* Depending on what the message is, it acts on it

--

* This might involve sending messages to other Actors

---

### Akka

*Akka* is an actor library written in Scala but with good Java bindings. Play is built on top of it it.

Defining Actors is rather easy:

--

```scala
class Hello extends Actor {
  def receive = {
    case NameMessage(name) =>
      println(s"Hello $name")
  }
}
```

---

### Akka in Java

Akka in Java isn't much more complicated

--

```java
public class Hello extends UntypedActor {

  public void onReceive(Object message) = {
    if (message instanceOf NameMessage) =>
      System.out.println("Hello " + ((NameMessage)message).name);
  }
}
```


---

### Akka

Creating Actors has a little wrinkle

```java
  ActorSystem system = ActorSystem.create("PingPongSystem");
  ActorRef hello = system.actorOf(Props.create(Hello.class));
```

What's `Props`?

---

### Props

* Actors don't have direct references to each other -- the other Actor could be on another machine.

* Instead they have an `ActorRef` -- the system will actually handle getting the message to its destination

* This also means we *can't directly call the constructor to create the actor* -- Akka has to do it for us

* `Props` is the properties (arguments) that need to be passed to the constructor of an actor we want to create

---

### Props

```scala
  val system = ActorSystem("PingPongSystem")
  val hello = system.actorOf(Props[Hello], name = "hello")
```

*Create me a Hello actor, called "hello", and its constructor doesn't take any arguments*

---

### Props

```java
  ActorSystem system = ActorSystem.create("PingPongSystem");
  ActorRef hello = system.actorOf(Props.create(Hello.class));
```

---

## Sending a message

If we have an `ActorRef` to the Hello actor sending it a message is very simple:

--

```scala
pong.tell(NameMessage("World"), sender)
```

or, for short:

```scala
hello ! NameMessage("World")
```
---

## Sending a message in Java


```java
pong.tell(new NameMessage("World"), getSender());
```

---

## Asking a question

* Algernon writes Bertie a letter asking a question
* Some time later, Bertie reads the letter. He thinks about it a bit, and then writes a reply
* Some time later, Algernon reads the reply

We could implement this just using what we have so far.

--

But it's a fairly common *pattern*; maybe it could be more concise?

---

## Asking a question

When Algernon sends Bertie the question, *sometime in the future* he'll get a reply

```scala
val fResponse:Future[Any] = bertie ? QuestionMessage(myQuestion)
```

---

## Asking a question in Java

```java
Timeout timeout = new Timeout(Duration.create(5, "seconds"));
Future<Object> future = Patterns.ask(actor, msg, timeout);
String result = (String) Await.result(future, timeout.duration());
```

---

## Ok, so what's a *Future*?

--

Why it's the thing I'll introduce in the next video, of course!