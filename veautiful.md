class: center, middle

# Veautiful

An open source Scala.js SPA framework from first principles

Will Billingsley

---

### Why show us this?

* So you can see how an SPA framework can be *built* not just used

* Conceptually, large frameworks can seem a mystery. This is small enough to be maintained by one person in their spare time!

* Small frameworks have an advantage for spare-time projects: they update *less* often, and being typed the changes from an upgrade are easier to find

---

## Veautiful Nodes

Let's start from first principles.

Suppose all we initially say is 

> There is such a thing as a Veautiful Node. When it is "attached" to the DOM, it has a corresponding DOM node which it manages.

![VNode](out/veautiful/vnode.svg)

---

## VNode

VNodes are responsible for managing their own DOM nodes. 

```scala
trait VNode {

  def domNode:Option[dom.Node]

  def beforeDetach():Unit = {}
  def detach():Unit
  def afterDetach():Unit = {}

  def beforeAttach():Unit = {}
  def attach():dom.Node
  def afterAttach():Unit = {}

  def isAttached:Boolean = domNode.nonEmpty

  var parent:Option[VNode] = None
}
```

This defines the *attachment lifecycle*.

---

## A simple VNode

```scala
case class Text(text:String) extends VNode {

  var domNode:Option[dom.Node] = None

  def create() = {
    dom.document.createTextNode(text)
  }

  def attach() = {
    val n = create()
    domNode = Some(n)
    n
  }

  def detach() = {
    domNode = None
  }

}
```

---

## VNode

Without much addition, *VNode* comes to represent any leaf node in the *virtual* DOM. 

For example:

* Text nodes

* Canvas elements

* Elements that will be managed using other libraries (e.g., d3.js)

As VNodes are completely responsible for managing their nodes (and any sub-nodes), we can let them vary.

---

## Make It So

React.js manages its children by *reconciling* the virtual DOM with the web page's dom, comparing this element-by-element and child-by-child.

But that doesn't tell us what to do with a canvas.

Let's generalise this...

--

```scala
trait MakeItSo {
  def makeItSo:PartialFunction[MakeItSo, _]
}
```

We now have a trait that any VNode can implement if you can give it a copy of itself, and it knows how to update itself to match.

---

### Make It So

Unlike JavaScript, Scala has a concept called `case classes`. These provide an easy way do define what makes two examples of a class *equal*.

```scala
case class Text(text:String) extends VNode
```

```scala
Text("The same") == Text("The same") // true
Text("The same") == Text("Not the same") // false
```

Only the state in the constructor is considered for equality. So, whether the VNode is attached is *not* considered part of equality

---

### Canvas and Make It So

We can define a component that can *Make it so* using a Canvas

```scala
case class Spacecraft(var x:Double, var y:Double, var r:Double) extends VNode with MakeItSo {

  def create() = dom.document.createElement("canvas")

  def redraw() = {
      // Code to redraw the spacecraft
  }

  override def afterAttach() = redraw()
  
  def makeItSo:PartialFunction[MakeItSo, _] = { 
    case SpaceCraft(xx, yy, dd) =>
      x = xx
      y = yy
      z = zz
      redraw()
  }


}
```

---

### Daddy Nodes

So far, `VNode` has no children. Let's define a `DNode` that has children.  

*Note* - The name `DNode` was an accident. It was called `DNode` because its methods were extracted from `DElement` that connected to a document element. But "daddy node" stuck.

![DNode](out/veautiful/dnode.svg)

A `DNode`'s chief responsibility is to handle the attachment lifecycle for its children.

---

### DNode

A `DNode`'s chief responsibility is to handle the attachment lifecycle for its children.

```scala
trait DNode extends VNode {

  def create():dom.Element
  var domNode:Option[dom.Element]

  def children:Seq[VNode]

  def attach() = {
    // ... Call beforeAttach, attach, and afterAttach for all children
  }

  def detach() = {
    // ... Call beforeDetach, detach, and afterDetach for all children  
  }

}
```

---

### DiffNode

Now let's define React-like nodes. They are `DNode`s that use a reconciliation algorithm so they can *make it so* including their children.

```scala
trait DiffNode extends DNode with MakeItSo {

  def makeItSo:PartialFunction[MakeItSo, _] = { case to:DiffNode =>
    updateSelf(to)
    updateChildren(to.children)
  }

  def updateChildren(to:Seq[VNode]):Unit = {
    // Use a diff algorithm to work out changes to make to children
    val diffReport = Differ.diffs(children, to)

    // implement changes
    Differ.processDiffs(this, diffReport.ops)
    children = diffReport.update    

    // Recurse down the DiffNode's (new) children, getting them to "make it so"
    children.iterator.zip(to.iterator) foreach {
      case (uu:MakeItSo, tt:MakeItSo) => uu.makeItSo(tt)
      case (u:Update, _) => u.update()
      case _ => // nothing to do
    }
  
}
```

---

### DElement

Finally, we're going to reach the point where can represent a *virtual DOM* node of a single DOM element.

`DElement` is a `DiffNode` that represents one HTML element.

```scala
case class DElement(
    name:String, 
    uniqEl:Any = "", 
    ns:String = DElement.htmlNS
) extends DiffNode {
    // etc
}
```

Something we get for free is the ability to set a unique ID on an element if we want it torn down rather than altered.

Elements will only be preserved if their *name*, *namespace*, and *uniqEl* are the same.

---

### A JSX-like DSL in Scala

Scala is a nice language for writing Domain Specific Languages, because you can create objects whose names are symbols.

```scala
object < {

  def apply(n:String, u:String = "", ns:String = DElement.htmlNS) = {
      DElement(n, u, ns)
  }
  
  def p = apply("div")
  def p = apply("p")

}
```

```scala
val para = <.div(
    <.p("This is a paragraph"),
    <.p("This is another paragraph")
)
```

---

## Components

Components are a fragment of the display tree. If our component is *pure* (stateless), we can just write a function:

```scala
def hello(name:String):VNode = <.p(s"hello $name")>
```

It's when we want our widgets to react to internal state that we need components

---

### DiffComponent


```scala
class Counter() extends DiffComponent {

  var number = 0

  def increment() = {
    number = number + 1;
    update()
  }

  override def render:DiffNode = <.div(
    s"The number is $number",
    <("button")(^.onClick --> increment, "Count!")
  )

}
```


---

## What this gives us 

This gives us

* React-like behaviour, where we have virtual trees of elements that can be reconciled using diff

* But we can insert any other kind of `VNode` wherever we want. E.g., inserting nodes that represent canvases, parts of the page that are managed by d3.js, etc.

---

## Routing in Veautful

As any `DNode` can manage its children however it wishes, a router simply becomes a node whose children change depending on the current state

For convenience, let's first define an `ElementComponent` - a stateful component that uses a `DElement` (a React-like subtree) to update its children.

```scala
class ElementComponent(val el:DElement) extends VNode {

  def domNode = el.domNode

  def attach() = {
    if (isAttached) {
      throw new IllegalStateException("Attached twice")
    }
    el.attach()
  }

  def detach() = el.detach()

  def renderElements(ch:VNode) = el.updateChildren(Seq(ch))
}
```

---

### HistoryRouter

For our router, we're going to need:

* A top-level element to render the view within:

    ```scala
    abstract class HistoryRouter[Route] extends ElementComponent(<.div) {
    ```

--

* A way of translating a path within the URL to a Route

    ```scala
    def routeFromLocation():Route
    ```

--

* A way of translating a Route object within the app to a path

    ```scala
    def path(route:Route):String
    ```

--

* A way of working out what to show in the view, depending on the current state

    ```scala
    def render:VNode
    ```

---

### HistoryRouter

```scala
abstract class HistoryRouter[Route] extends ElementComponent(<.div) {

  var route:Route

  def path(route:Route):String

  def routeFromLocation():Route

  def render:VNode

  def registerHistoryListeners():Unit = {
    dom.window.onpopstate = {
      event =>
        route = routeFromLocation()
        renderElements(render)
    }
  }

  def routeTo(r:Route):Unit = {
    route = r
    val p = path(route)
    renderElements(render)
  }

}
```

---

### Paths and Routes

* *Paths* are strings. E.g.: `"#/class/123"`. We need to turn them into routes

* *Routes* are objects. E.g.: `ClassRoute(123)`. We need to turn them into strings

---

### Converting paths to routes is string matching

Converting paths to routes is just a matter of splitting the string into its components, in such a way that the programming language can match on it

```scala
def pathArray(pathname:String = location.pathname): Array[String] = {
    pathname.drop(1).split('/').map(js.URIUtils.decodeURI)
}
```

```scala
pathArray() match {
    case Array("class", classId) => ClassRoute(classId.toInt) 
}
```

---

### Converting routes to paths is string interpolation

Converting a route object to a path is easier - it's just a `stringify` method. But we can make it easier with a DSL

```scala
route match {
    case IntroRoute => (/# / "").stringify
    case ToDoRoute => (/# / "todo").stringify
    case ReactLikeRoute => (/# / "reactLike").stringify
}
```

---

### Define the Routes

First we define objects to represent our routes

```scala
sealed trait ExampleRoute
case object IntroRoute extends ExampleRoute
case object ToDoRoute extends ExampleRoute
case object ReactLikeRoute extends ExampleRoute
```

---

### Then we declare a Router

And define how to render each route - it's just a reference to another component

```scala
object Router extends HistoryRouter[ExampleRoute] {

  var route:ExampleRoute = IntroRoute

  def render() = {
    route match {
      case IntroRoute => Intro.page
      case ToDoRoute => ToDoList.page
      case ReactLikeRoute => ReactLike.page
    }
  }

```

---

### Define how to get a path from a route

For the page URL, we want to know what path to show

```scala
  override def path(route: ExampleRoute): String = {
    import PathDSL._

    route match {
      case IntroRoute => (/# / "").stringify
      case ToDoRoute => (/# / "todo").stringify
      case ReactLikeRoute => (/# / "reactLike").stringify
    }
  }
```

---

### Define how to get a route from a path

To handle forward and back, we then work out how to parse the window location into the routes

```scala
  override def routeFromLocation(): ExampleRoute = PathDSL.hashPathArray() match {
    case Array("") => IntroRoute
    case Array("todo") => ToDoRoute
    case Array("reactLike") => ReactLikeRoute
    case x =>
      println(s"path was ${x}")
      IntroRoute
  }

}
```

---

class: bottom

Written by Will Billingsley

<small>
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.</small>