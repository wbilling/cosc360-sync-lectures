class: center, middle

# An unusually detailed exam guide

---

## Why quite so detailed

This year has been challenging enough for students already:

* The pre-requisite (COSC260) hadn't run yet, so this has needed to cover server and client, rather than just the real-time client-side technologies

* A late change of coordinator, for a new course, meaning it's been whipped together as we go

* A drop to eleven-week terms, making everything that bit more compressed and hurried with no "swotvac" 

* Lots of new technologies 

---

## Why quite so detailed

I've also noticed that some students find it hard to switch between the project and exam revision -- suddenly the assessment format is all different.

* Moral of the story: Don't necessarily expect every unit to tell you exactly which topics will be chosen for the exam! But on this one, we will...

---

## Overall format

* A small number of questions, each with many parts

* The tutorials are *a very good guide* to what the format of questions is like

* COSC560 has an extra question

---

## Protocols

* Always on the protocols (HTTP, EventSource, WebSockets). In the past this has given you the text of a request and asked you to explain it.

* This year you will be asked to explain the differences between protocols... (what they do, some key headers, etc)

* And give a short example of the client-side code to make some sorts of request

* (So that could be XMLHttpRequest-driven HTTP, WebSockets, EventSource)

---

## Web Security

* Know your threats

* Know features of HTTP, browsers, and the protocols we've talked about that are about security and protecting against threats

---

## Design question

* The system you are designing for will be different, but the format will be very similar to the tutorial question

---

## Client-side rendering

* You will need to understand how each of these works: React.js, Angular.js, d3.js, Canvas, SVG, WebGL

* There is a question on WebGL. Highlighted because it was the last lecture (often missed)

    - It's not quite the same as the text-on-cube example, but you should understand the process of what that's doing.
    
    - *You don't have to write the code*, just explain how it would work.

---

## Compile-to-JavaScript

* Fairly common for me to ask questions about the process of turning TypeScript, JSX, CoffeeScript, etc into *minified* JavaScript, and what *sourcemaps* are.

---

## COSC360/560

* COSC360 has three of these questions

* COSC560 has all four.

