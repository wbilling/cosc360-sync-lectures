/*
 * The whole application goes in the my-example module, which depends on ng-route
 * so that we bring in the router
 */
var module = angular.module("my-example", ['ngRoute'])
  .config(function($routeProvider, $locationProvider) {
    /*
     * Though we're using the router to show it still works, actually we're just
     * rendering a default route
     */
    $routeProvider     
    .otherwise({
        templateUrl: "page.html",
        controller: "pageController"
    })
  
    /*
     * Turn HTML5 mode off, as we don't have equivalent routes on the server and 
     * refresh would give us a 404
     */
    $locationProvider.html5Mode(false);
  });

// Our controllers are all defined on our one module
module.controller("pageController", function($scope) {
    console.log("page controller created")

    // Function wired into the "add point" button
    $scope.addPoint = function() {
        console.log("called")

        var x = Math.random() * 60;
        var y = Math.random() * 60;

        // Note that we are pushing elements into the array, not re-assigning
        // window.points. As later we've set $scope.points = window.points
        // in some directives, if we were to reassign window.points, it'd break
        window.points.push({ x: x, y: y })
    }
})

// Defines the <points-list> tag
module.component("pointsList", {
  templateUrl: "pointsList.html",
  controller: function($scope) {
    $scope.points = window.points;
  }
})

// Defines the <canvas-control> tag
module.directive("canvasControl",  function() {
    return {
        templateUrl: "canvasControl.html",
        controller: function($scope) {
            // We want to watch this array, so we'll need to set it on $scope
            $scope.points = window.points;

            // Our canvas drawing function
            $scope.draw = function() {
                console.log("drawing called")

                // $scope.el is going to get set by our link function.
                // meaning it won't always be defined, but only after the controller
                // has been linked to the elements inserted into the page
                if ($scope.el) {
                    console.log("drawing to canvas element")
                    var context2d = $scope.el.getContext("2d")

                    context2d.clearRect(0, 0, $scope.el.width, $scope.el.height);

                    context2d.beginPath();
                    context2d.moveTo(0,0);
                    $scope.points.forEach(function (value) {
                        context2d.lineTo(value.x, value.y);                
                    })
                    context2d.stroke();
                }
            }

            // "Deep watch" the points array, and call draw whenever its content changes. Note the "true" in the last param!
            $scope.$watch('points', function(newValue, oldValue) {
                $scope.draw();
            }, true);


        },

        // The link function is called after the template has been inserted, when the
        // controller is being "linked" to it. At this point, the canvas is there
        // to be found
        link: function($scope, $element) {
            console.log("Linking canvas");

            // Find the canvas element within our template
            var el = $element.find("canvas")

            // As Angular uses jqLite, we'll get a wrapped version of the element
            // [0] will get the DOM element
            $scope.el = el[0]

            // call our draw function
            $scope.draw()
        }
    }
})



window.points = [
  { x: 12, y: 20 },
  { x: 22, y: 30 }
]

