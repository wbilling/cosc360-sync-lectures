class: center, middle

.header[![UNE logo](https://www.une.edu.au/__data/assets/image/0011/344/une-logo.png?v=0.1.6)]

# OAuth

Week 10 Lecture 1

.footnote[<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a>&nbsp; Written and presented by Will Billingsley]

---

### Social login

How can we approve **Site A** to work with my account on **Site B** **without** giving Site A access to my **password** on Site B?

eg: 

* Authorise an app to make Tweets for me?

* Log into Assessory using my GitHub account?

OAuth is a solution to this.

---

### A bit of history

How many sites do you have a log in to?

--

Do you re-use your password between sites?

--

Uh-oh... [Have I been pwned](https://haveibeenpwned.com/PwnedWebsites)

---

<img src="http://openid.net/wordpress-content/uploads/2014/09/openid-r-logo-900x360.png" height="100px" />

What if you had a single login that *you* controlled ...

--

... and you could use that login to log into any number of other sites without giving them your password?

--

2005 (obsolete) version:

* Your login was a URL, eg:  
  `http://www.livejournal.com/users/fred`

* You could get an OpenID from dedicated sites (eg, myopenid.com) or using your existing logins (eg, LiveJournal, Blogger, ...)

---

### What happened

* OpenID popular for a while among developers. Not always a great experience for users.  

  ![](http://jvance.com/media/2009/02/10/JQueryOpenIdPlugin9.media)
  
* *Log in by pasting in a long URL???*

* *How many* login buttons are there on that page?  

---

### Beginnings of OAuth

* 2006, Twitter wanted to combine OpenID with the idea of a *valet key*.  

--

  How can I authorise another app to *do some things* but *not to do other things*.

--

  Also ended up with a better social sign-in experience  

  ![Sign in with Twitter](https://g.twimg.com/dev/sites/default/files/images_documentation/sign-in-with-twitter-gray.png)


---

### Granting access

![](https://camo.githubusercontent.com/8db7c76861f30b17d11e8dd297056d11418bb640/68747470733a2f2f662e636c6f75642e6769746875622e636f6d2f6173736574732f3836352f323235313835332f35663537383239382d396461312d313165332d396364612d3639376530643166323930352e706e67)

---

### Since then...

There are now several versions

* OAuth 1.1

  - somewhat complex

* OAuth 2.0

  - simpler, we'll focus on

* OpenID Connect 

  - built on top of OAuth 2.0

---

### The basic idea (web flow)

![](oauth/oauth2.svg)

---

### Suppose you were going to sign into Assessory using your GitHub account

1. You'd visit a log-in page on Assessory, that has a "sign in with GitHub" button. Clicking the button would make a POST request to Assessory. Assessory would choose a random "state" string, and return a *redirect* to your browser, pointing to GitHub's OAuth login page with that state and Assessory's client ID in the request. 

3. You log-in and interact with your GitHub account on GitHub. Once you've finished, GitHub redirects your browser back to Assessory, with a code it has issued as well as the state.

5. The redirect causes your browser to send these to the Assessory server.
Assessory checks the state is present, and then talks to GitHub's server to verify the code and exchange it for an access token.

7. Assessory then uses the access token to request your user details from GitHub, getting the username etc you've just logged in as. Then it can update your session as being logged in.

---

### Setting up OAuth

Usually, there's a few things to do:

* Register an app on GitHub / Twitter / Google / etc

  They will allocate you a **client ID** and a **client secret**  

  You need to set the **callback URL**  

* Set up routes in your server for  

  - the login form
  - the POST request to initiate the process
  - the callback GET request

---

### Bearer authentication

Typically, we have only seen requests from browser to server, using a cookie containing a session key for authentication.

When Assessory calls GitHub to get your user details, it uses a token not a cookie.

This involves setting the `Authorization` request header. Typically it may look something like:

```http
Authorization: Bearer ca9av23nr32426
```

or

```http
Authorization: token asdfas987ada03n
```

---

### JSON Web Tokens

The process we've seen lets Assessory's *server* make requests under a user's authorisation.

But what if we would like the *browser* to be able to make requests across sites under a user's authorisation?

We need to get some kind of secure token up to the browser...

---

### JSON Web Tokens

* Token in JSON format (nicely readable from browser and almost anywhere)

* then *Base64url* encoded to form a compact string

* then *digitally signed* using a cryptographic algorithm

Typically end up in this format

```
header.body.signature
```

---

### Example, from [jwt.io](https://jwt.io)

Encoded:

```
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.
eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.
TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ*
```

Decoded header:

```json
{
  "alg": "HS256",
  "typ": "JWT"
}
```

Decoded body: 

```json
{
  "sub": "1234567890",
  "name": "John Doe",
  "admin": true
}
```

---

### JWT advantages

Theoretically, you can do things like serverless single-sign on.

* Have an identity server, that issues JSON Web Tokens including a date and expiry

* As the JWT is signed against a secret, it can be posted as evidence you are logged in, and validated by servers that know the secret.

Auth0 is a service that uses this for single-sign on.

But, there are always some [Security caveats](https://auth0.com/blog/critical-vulnerabilities-in-json-web-token-libraries/)

---

### OpenID Connect

We started with OpenID. Let's close the loop.

You recall how after it gets an authentication token, Assessory makes a request to GitHub to get your user details?

OpenID Connect does something very similar -- after the authentication token is given, it makes a request to an endpoint...

... which returns the user identity as a JSON Web Token

```json
  {
   "iss": "https://server.example.com",
   "sub": "24400320",
   "aud": "s6BhdRkqt3",
   "nonce": "n-0S6_WzA2Mj",
   "exp": 1311281970,
   "iat": 1311280970,
   "auth_time": 1311280969,
   "acr": "urn:mace:incommon:iap:silver"
  }
```

---

### OpenID Connect 

Diagram from their RFC: 

```
+--------+                                   +--------+
|        |                                   |        |
|        |---------(1) AuthN Request-------->|        |
|        |                                   |        |
|        |  +--------+                       |        |
|        |  |        |                       |        |
|        |  |  End-  |<--(2) AuthN & AuthZ-->|        |
|        |  |  User  |                       |        |
|   RP   |  |        |                       |   OP   |
|        |  +--------+                       |        |
|        |                                   |        |
|        |<--------(3) AuthN Response--------|        |
|        |                                   |        |
|        |---------(4) UserInfo Request----->|        |
|        |                                   |        |
|        |<--------(5) UserInfo Response-----|        |
|        |                                   |        |
+--------+                                   +--------+
```



---

.footnote[<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>]

