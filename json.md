
class: center, middle

# JavaScript Obect Notation (JSON)

---

### XML APIs are verbose and awkward to read

```xml
<?xml version="1.0" encoding="ISO-8859-1"?>
<methodResponse>
   <params>
      <param>
         <value><int>30</int></value>
      </param>
   </params>
</methodResponse>
```

---

### JSON can be much shorter

```
{ "result": 3 }
```

* Syntax is the object literal syntax from JavaScript

---

### Can be parsed using eval

```js
var result = somethingThatReturnsJson()
var resultObj = eval(result)
```

* This made JSON easy for web programmers to work with

---

### But don't parse it using eval

```js
var result = somethingThatReturnsJson()

// result happens to be the string "doSomethingNasty()"
var resultObj = eval(result)
```

* `eval()` is something of a security risk. Browsers now have `JSON.parse()`

```js
var resultObj = JSON.parse(result)
```

---

### So what is valid JSON?

1: JSON objects

  ```js
  { "id": "2",
      "name": "Bertie",
      "extra": {
        "lastSeen": "Wednesday"
      }
  }
  ```
  
---

### So what is valid JSON?

2: JSON arrays

  ```js
  [ 1, 2, 3, 4 ]
  ```
  
  ```js
  [
      { "id": "1", "name": "Algernon" },
      { "id": "2", 
        "name": "Bertie", 
        "extra": { 
          "lastSeen": "Wednesday"
        } 
      }
  ]
  ```

---

### JSON API examples

* [Twitter API](https://dev.twitter.com/rest/reference/get/users/show)

* [GitHub API](https://developer.github.com/v3/users/)

---

### Parsing JSON in Play framework for Java

```java
import com.fasterxml.jackson.databind.JsonNode;
```

```java
JsonNode json = result().body().asJSON();
String name = json.findPath("name").textValue();
```

---

### Composing JSON in Play for Java

```java
import play.libs.Json;
```


```java
ObjectNode result = Json.newObject();
result.put("exampleField1", "foobar");
result.put("exampleField2", "Hello world!");
return ok(result);
```

---

### Parsing JSON in Play for Scala

Generally, use a *body parser*

```scala
def receiveJsonPost():Action[JsValue] = Action(bodyParser = parse.json) { req =>

    val json:JsValue = req.body

    /*
     * If the JSON is { name: "Fred" }
     * then this gets the value that's in name
     */
    val name = (json \ "name").as[String]

    Ok(s"Hello $name")
  }
```

---

### Composing JSON in Play for Scala

The Play JSON library has a good concise notation for this:

```scala
Json.obj(
  "Hello" -> Seq("World", "Bob", "Alice"),
  "Goodbye" -> Json.obj(
    "Mr" -> "Chips"
  )
)
```

---

### Parsing JSON in express

Generally, use a body parser!

```ts
import bodyParser from "body-parser";
```
```ts
app.use(bodyParser.json())
```

---

### Composing JSON in express

Most often, you can output it directly:

```ts
resp.send({
        "Hello": ["World", "Alice", "Bob"],
        "Goodbye": {
            "Mr": "Chips"
        }
    })
```

But you can use JSON.stringify