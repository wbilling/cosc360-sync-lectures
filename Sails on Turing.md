# Sails on Turing

To run Sails on Turing, there's a few small extra tasks we need to do:

1. Tell npm (Node Package Manager) how to deal with UNE's proxy.
1. Download the Sails npm module
1. Get you a unique port number, so your server doesn't clash with other students'

This is the first year we've used Sails or Node, so bear with us if we need to debug anything.



## 1. Telling NMP how to deal with UNE's proxy

** *Only needed if you're working on turing* **

Fortunately, NPM respects the Linux environment variables for your proxy username and password. So the script that our sysadmin has already written for setting your proxy details should work for NPM too:

```
setproxy
```

### 2. Getting Sails

Sails.js is not installed by default, and unlike on your own computer, you don't have the permissions to use the `-g` flag to install things globally on Turing. 

So we're going to need to ask you to install it locally. Let's create a `sails` directory so we don't mess up your home directory too much.

```
cd ~
mkdir sails
cd sails
```

Now let's install Sails (and all its dependencies) here

```
npm install sails
```

This will create a subdirectory called `node-modules` which will then have rather a lot in it. You might get an error at the end of the install, but things seem still to work...

    
## 3. Find out your port number

** *Only needed if you're working on turing* **

As there are lots of students on turing, we need to give each of you a unique port number

```
my_tomcat_ports
```

Note down the HTTP port you have been allocated. (We're not using Tomcat, but we'll use that port). 

Later in this tutorial, wherever you see **MYPORT**, replace it with your HTTP port number

    
## 4. Creating a new Project



And now we can create a new project

```
~/sails/node_modules/sails/bin/sails.js new test-project
```

This should create a new subdirectory with a default project in it.


### Starting the server

let's have a look at what you've cloned

```
ls
```

You should see a relatively small number of files and directories.

* `api`, `assets`, `config`, `tasks`, `views`

* `Gruntfile.js` contains information for Grunt (a build system)

* `node-modules` containing, amongst other things, Sails! (But we had to do those previous stepts to get it there)

Let's start the server.

2. Sails lift is the command that starts your project running. 

    ```
    ./node-modules/sails/bin/sails.js lift --port=MYPORT
    ```
    
   remembering to replace MYPORT with your port number
   
   The server should start very quickly -- JavaScript isn't a compiled langauge (it's JIT-compiled by the virtual machine after a few runs interpreting it), so there's nothing to stop it from coming up almost instantly    
    
### Curling our server

We're going to need to tell curl not to go through the web proxy for the requests we make to our own server.

Open a new shell to make these requests, but keep an eye on what's happening in the shell that is running Play

1. Call the server using curl

    ```
    curl -x "" -v localhost:MYPORT
    ```

    (remembering to replace $myport with your HTTP port number)
   

3. Open a web browser, and try visiting those URLs through the browser    

3. Have a look through all the application code. Does any of it look big or is it mostly small files with not much code in?
   
### Stop the server

1. Stop the server from running by `Ctrl-C`
